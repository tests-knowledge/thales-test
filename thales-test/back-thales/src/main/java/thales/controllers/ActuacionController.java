package thales.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import thales.dto.ActuacionDTO;
import thales.entities.commons.Actuacion;
import thales.services.IActuacionService;
import thales.utilidades.ApiResponse;
import thales.utilidades.Constantes;
import thales.utilidades.ConstantesDeMensajes;
import thales.utilidades.UtilidadesMensajesApi;


@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE }, maxAge = 3600)
@RequestMapping("/actuacion")
public class ActuacionController {

	@Autowired
	private IActuacionService iactuacionService;
	
	@Autowired
	UtilidadesMensajesApi utilidadesMensajesApi;
	public static final Logger LOGGER = LoggerFactory.getLogger(ActuacionController.class);
	
	
	/**
	 * Devuelve una ActuacionDTO vinculada a un codiActu pasado como parametro
	 * 
	 * @param codiActu
	 * @author Ingeniero Gabriel Reyes.
	 * @return
	 */
	@Operation(summary = "Buscar por codiActu una ActuacionDTO", description = "Devuelve una ActuacionDTO con ESTADO_REGISTRO_ACTIVO vinculado a un codiActu pasado como parametro")
	@GetMapping("/{codiActu}")
	public ApiResponse<ActuacionDTO> findById(@PathVariable Short codiActu) {
		try {
			Optional<Actuacion> actuacion = iactuacionService.findById(codiActu);
			if (actuacion.isPresent()) {
				return new ApiResponse<>(HttpStatus.OK.value(), ConstantesDeMensajes.INFORMACION_ENCONTRADA, actuacion.get().toBO());			
			} else {
				return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), ConstantesDeMensajes.INFORMACION_NO_ENCONTRADA, null);
			}
		} catch (Exception e) {
			return new ApiResponse<>(HttpStatus.CONFLICT.value(), e.getMessage(), ConstantesDeMensajes.ERROR_PETICION);
		}
	}
	
	/**
	 * Devuelve una lista que contiene todos los ActuacionDTO 
	 * 
	 * @author Ingeniero Gabriel Reyes.
	 * @return
	 */
	@Operation(summary = "Listar ActuacionDTO", description = "Devuelve una lista que contiene todos los ActuacionDTO con estaActu AC")
	@GetMapping("/")
	public ApiResponse<List<ActuacionDTO>> listarTodos() {
		try { 
			List<ActuacionDTO> listaActuacionDTO = new ArrayList<>();
			List<Actuacion> listaActuacion = iactuacionService.getListaActuacionesPorEstado(Constantes.ESTADO_REGISTRO_ACTIVO);
			if (!listaActuacion.isEmpty()) {
				listaActuacion.forEach(actuacion -> listaActuacionDTO.add(actuacion.toBO()));
			}
			return new ApiResponse<>(HttpStatus.OK.value(), ConstantesDeMensajes.LISTA_CARGADA, listaActuacionDTO);			
		} catch (Exception e) {
			return new ApiResponse<>(HttpStatus.CONFLICT.value(), e.getMessage(), ConstantesDeMensajes.ERROR_PETICION);
		}
	}
	
	/**
	 * Elimina un ActuacionDTO
	 * 
	 * @param ActuacionDTO
	 * @author Ingeniero Gabriel Reyes.
	 * @return
	 */	
	@Operation(summary = "Elimina un ActuacionDTO", description = "Elimina un ActuacionDTO actualizando el estado a ESTADO_REGISTRO_INACTIVO")
	@DeleteMapping("/")
	public ApiResponse<String> eliminar(@RequestBody ActuacionDTO actuacionDTO) {
		try {
			if (actuacionDTO != null) {
				Optional<Actuacion> actuacion = iactuacionService.findById(actuacionDTO.getCodiActu());
				if (actuacion.isPresent()) {
					actuacion.get().setEstaActu(Constantes.ESTADO_REGISTRO_INACTIVO);
					iactuacionService.save(actuacion.get());
					return new ApiResponse<>(HttpStatus.OK.value(), ConstantesDeMensajes.ELIMINADO_EXITOSO, actuacion.get().toBO());			
				}
			}
			return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), ConstantesDeMensajes.INFORMACION_NO_ENCONTRADA, null);
		} catch (Exception e) {
			return new ApiResponse<>(HttpStatus.CONFLICT.value(), e.getMessage(), ConstantesDeMensajes.ERROR_PETICION);
		}
	}
	
	/**
	 * Guarda o Actualiza un ActuacionDTO
	 * 
	 * @param actuacionDTO
	 * @author Ingeniero Gabriel Reyes.
	 * @return
	 */
	@Operation(summary = "Guarda o Actualiza un ActuacionDTO", description = "Guarda o Actualizar un ActuacionDTO")
	@PostMapping("/")	
	public ApiResponse<ActuacionDTO> guardar(@RequestBody ActuacionDTO actuacionDTO) {
		String mensajeRespuesta = ConstantesDeMensajes.GUARDADO_EXITOSO;
		try {
			if (actuacionDTO != null) {	
				if(actuacionDTO.getCodiActu() != null) {
					Optional<Actuacion> actuacion = iactuacionService.findById(actuacionDTO.getCodiActu());
					if (actuacion.isPresent()) {
						mensajeRespuesta = ConstantesDeMensajes.ACTUALIZADO_EXITOSO;					
					}
				} else {
					actuacionDTO.setCodiActu(iactuacionService.getIdConsecutivo());
				}
				iactuacionService.save(actuacionDTO.toBO());  
				return new ApiResponse<>(HttpStatus.OK.value(), mensajeRespuesta, actuacionDTO);			
			}
			return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), ConstantesDeMensajes.INFORMACION_NO_ENCONTRADA, null);
		} catch (Exception e) {
			return new ApiResponse<>(HttpStatus.CONFLICT.value(), e.getMessage(), ConstantesDeMensajes.ERROR_PETICION);
		}
	}
}
