package thales.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import thales.dto.ActuacionDTO;
import thales.dto.EmployeeDTO;
import thales.entities.commons.Actuacion;
import thales.entities.commons.Employee;
import thales.services.IActuacionService;
import thales.services.IEmployeeService;
import thales.utilidades.ApiResponse;
import thales.utilidades.Constantes;
import thales.utilidades.ConstantesDeMensajes;
import thales.utilidades.UtilidadesMensajesApi;


@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,
		RequestMethod.DELETE }, maxAge = 3600)
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private IActuacionService iactuacionService;
	
	@Autowired
	private IEmployeeService iemployeeService;
	
	@Autowired
	UtilidadesMensajesApi utilidadesMensajesApi;
	public static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
	
	
	/**
	 * Devuelve un EmployeeDTO vinculado a un id pasado como parametro
	 * 
	 * @param id
	 * @author Gabriel Reyes.
	 * @return
	 */
	@Operation(summary = "Buscar por id un EmployeeDTO", description = "Devuelve un EmployeeDTO vinculado a un id pasado como parametro")
	@GetMapping("/{idEmployee}")
	public ApiResponse<EmployeeDTO> findById(@PathVariable Short idEmployee) {
		try {
			Employee employee = iemployeeService.getEmployeeById(idEmployee);
			EmployeeDTO employeeDto = null;
			if(employee != null) {
				employeeDto = employee.toBO();
				employeeDto = iemployeeService.calcAnualSalary(employeeDto);
			}
				return new ApiResponse<>(HttpStatus.OK.value(), ConstantesDeMensajes.INFORMACION_ENCONTRADA, employeeDto);	
		} catch (Exception e) {
			return new ApiResponse<>(HttpStatus.CONFLICT.value(), e.getMessage(), ConstantesDeMensajes.ERROR_PETICION);
		}
	}
	
	/**
	 * Devuelve una lista que contiene todos los EmployeeDTO 
	 * 
	 * @author Gabriel Reyes.
	 * @return
	 */
	@Operation(summary = "Listar EmployeeDTO", description = "Devuelve una lista que contiene todos los EmployeeDTO")
	@GetMapping("/")
	public ApiResponse<List<EmployeeDTO>> listarTodos() {
		System.out.println("Entra en EmployeeDTO listarTodos");
		try { 
			List<EmployeeDTO> listaEmployeeDTO = new ArrayList<>();
			List<Employee> listaEmployee = iemployeeService.getListaEmployees();
//			listaEmployeeDTO = iemployeeService.getListaEmployees();
			if (!listaEmployee.isEmpty()) {
				listaEmployee.forEach(employee -> {
					EmployeeDTO employeeDto = employee.toBO();
					employeeDto = iemployeeService.calcAnualSalary(employeeDto);
					listaEmployeeDTO.add(employeeDto);
					});
//				listaEmployeeDTO.add(employee.toBO())
			}
			return new ApiResponse<>(HttpStatus.OK.value(), ConstantesDeMensajes.LISTA_CARGADA, listaEmployeeDTO);			
		} catch (Exception e) {
			return new ApiResponse<>(HttpStatus.CONFLICT.value(), e.getMessage(), ConstantesDeMensajes.ERROR_PETICION);
		}
	}
}
