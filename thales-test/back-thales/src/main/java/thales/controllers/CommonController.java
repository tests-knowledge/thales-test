package thales.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.ValidationException;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import thales.services.CommonService;


/**
 * 
 * @author c.cgamboa
 *
 * @param <E> Endidad
 * @param <S> Servicio
 * @param <I> tipo llave primaria
 */
public class CommonController <E, S extends CommonService<E, I>, I> {

	@Autowired 
    private Validator validator; 
	
	
	@Autowired
	protected S servicio;
	/*
	@GetMapping
	public ResponseEntity<?> listar(){
		return ResponseEntity.ok().body(servicio.findAll());
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminar(@PathVariable I id){
		servicio.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	*/
	@GetMapping("/pagina")
	public ResponseEntity<?> listar(Pageable pageable){
		return ResponseEntity.ok().body(servicio.findAll(pageable));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> buscarPorId(@PathVariable I id){
		Optional<E> o = servicio.findById(id);
		if(!o.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(o.get());
	}
	
	@PostMapping
	public ResponseEntity<?> guardar(@Valid @RequestBody E entity, BindingResult result){
		
		if(result.hasErrors()) {
			return this.validar(result);
		}
		E entityDb = servicio.save(entity);
		return ResponseEntity.status(HttpStatus.CREATED).body(entityDb);
	}
	
	
	protected ResponseEntity<?> validar(BindingResult result){
		Map<String, Object> errores = new HashMap<>();
		result.getFieldErrors().forEach(err -> {
			//" " + err.getField() + " "+
			errores.put(err.getField(),   err.getDefaultMessage());
		});
		return ResponseEntity.badRequest().body(errores);
	}
	
	/** 
	 *  muy mortal este metodo, el bindin se hace sobre la entidad, no sobre el RequesBody, fuckyea. Asi puedo recibir un DTO y luego evaluar si es correcto o no su dato de acuerdo a la <E> Entidad
	 * <p> Validates the given Entity variable and throws validation exceptions based on the type of error. 
	 * If the error is standard bean validation errors then it will throw a ConstraintValidationException with the set of the constraints violated. </p> 
	 * <p> If the error is caused because an existing member with the same email is registered it throws a regular validation exception so 
	 * that it can be interpreted separately. </p>
	 * @param entity Entity to be validated
	 * @author Ing Carlos Gamboa..
	 * @throws ConstraintViolationException If Bean Validation errors exist
	 * @throws ValidationException If member with the same email already exists
	 */
	protected Map<Object, String> validar(E entity) {
		//throws ConstraintViolationException, ValidationException 
	  Set<ConstraintViolation<E>> violations=validator.validate(entity);
	  Map<Object, String> validationResult = new HashMap<>();
	  if (!violations.isEmpty()) {
		  violations.forEach( violation->{			 
			  validationResult.put(violation.getPropertyPath(), violation.getMessage()+". Valor recibido: "+violation.getInvalidValue());
		  });
		  return validationResult;
	   
	  }
	  return null;
	 	 
	}
	
	/** 
	 * Aca validamos el id de la enntidad, ya que en las entidades vienen llaves compuestas aveces..
	 * <p> Validates the given Member variable and throws validation exceptions based on the type of error. If the error is standard bean validation errors then it will throw a ConstraintValidationException with the set of the constraints violated. </p> <p> If the error is caused because an existing member with the same email is registered it throws a regular validation exception so that it can be interpreted separately. </p>
	 * @param member Member to be validated
	 * @author Ing Carlos Gamboa.. muy mortal este metodo, el bindin se hace sobre la entidad, no sobre el RequesBody, fuckyea
	 * @throws ConstraintViolationException If Bean Validation errors exist
	 * @throws ValidationException If member with the same email already exists
	 */
	protected Map<Object, String> validarPorId(I id) {
		//throws ConstraintViolationException, ValidationException 
	  Set<ConstraintViolation<I>> violations=validator.validate(id);
	  Map<Object, String> validationResult = new HashMap<>();
	  int i=0;
	  if (!violations.isEmpty()) {
		  violations.forEach( violation->{			 
			  System.out.println("Validacion "+i+"->"+violation);
			  validationResult.put(violation.getPropertyPath(), violation.getMessage()+". Valor recibido: "+violation.getInvalidValue());
		  });
		  return validationResult;
	   
	  }
	  return null;
	 	 
	}
}
