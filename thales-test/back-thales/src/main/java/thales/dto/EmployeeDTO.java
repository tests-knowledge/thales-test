package thales.dto;

import thales.entities.commons.Employee;

public class EmployeeDTO {
	
	private Short idEmployee;
    private String employeeName;
    private float employeeSalary;
    private float employeeAnualSalary;
    private Integer employeeAge;
    private String profileImage;
    
    public EmployeeDTO() {
	}
	
	public EmployeeDTO(Short idEmployee, String employeeName, float employeeSalary, float employeeAnualSalary, Integer employeeAge,
			String profileImage) {
		super();
		this.idEmployee = idEmployee;
		this.employeeName = employeeName;
		this.employeeSalary = employeeSalary;
		this.employeeAge = employeeAge;
		this.profileImage = profileImage;
		this.employeeAnualSalary = employeeAnualSalary;
	}

	public Short getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Short idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public float getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(float employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	public Integer getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(Integer employeeAge) {
		this.employeeAge = employeeAge;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
	
	public float getEmployeeAnualSalary() {
		return employeeAnualSalary;
	}

	public void setEmployeeAnualSalary(float employeeAnualSalary) {
		this.employeeAnualSalary = employeeAnualSalary;
	}

	public Employee toBO() {
		Employee employee = new Employee();
		employee.setId(idEmployee);
		employee.setemployee_name(employeeName);
		employee.setemployee_salary(employeeSalary);
		employee.setemployee_age(employeeAge);
		employee.setprofile_image(profileImage);
		return employee;
	}

	@Override
	public String toString() {
		return "EmployeeDTO [idEmployee=" + idEmployee + ", employeeName=" + employeeName + ", employeeSalary="
				+ employeeSalary + ", employeeAnualSalary=" + employeeAnualSalary + ", employeeAge=" + employeeAge + ", profileImage=" + profileImage + "]";
	}

}
