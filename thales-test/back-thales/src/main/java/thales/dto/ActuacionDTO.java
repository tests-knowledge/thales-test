package thales.dto;

import java.util.Date;

import thales.entities.commons.Actuacion;

public class ActuacionDTO {
	
	private Short codiActu;
    private String tipoActu;
    private String nombActu;
    private String noabActu;
    private String descActu;
    private String estaActu;
    private Date fechVige;
    private Date fechCrea;
    private String noviActu;
    
    public ActuacionDTO() {
	}
    
	public ActuacionDTO(Short codiActu, String tipoActu, String nombActu, String noabActu, String descActu,
			String estaActu, Date fechVige, Date fechCrea, String noviActu) {
		this.codiActu = codiActu;
		this.tipoActu = tipoActu;
		this.nombActu = nombActu;
		this.noabActu = noabActu;
		this.descActu = descActu;
		this.estaActu = estaActu;
		this.fechVige = fechVige;
		this.fechCrea = fechCrea;
		this.noviActu = noviActu;
	}
	
	public Short getCodiActu() {
		return codiActu;
	}
	
	public void setCodiActu(Short codiActu) {
		this.codiActu = codiActu;
	}
	
	public String getTipoActu() {
		return tipoActu;
	}
	
	public void setTipoActu(String tipoActu) {
		this.tipoActu = tipoActu;
	}
	
	public String getNombActu() {
		return nombActu;
	}
	
	public void setNombActu(String nombActu) {
		this.nombActu = nombActu;
	}
	
	public String getNoabActu() {
		return noabActu;
	}
	
	public void setNoabActu(String noabActu) {
		this.noabActu = noabActu;
	}
	
	public String getDescActu() {
		return descActu;
	}
	
	public void setDescActu(String descActu) {
		this.descActu = descActu;
	}
	
	public String getEstaActu() {
		return estaActu;
	}
	
	public void setEstaActu(String estaActu) {
		this.estaActu = estaActu;
	}
	
	public Date getFechVige() {
		return fechVige;
	}
	
	public void setFechVige(Date fechVige) {
		this.fechVige = fechVige;
	}
	
	public Date getFechCrea() {
		return fechCrea;
	}
	
	public void setFechCrea(Date fechCrea) {
		this.fechCrea = fechCrea;
	}
	
	public String getNoviActu() {
		return noviActu;
	}
	
	public void setNoviActu(String noviActu) {
		this.noviActu = noviActu;
	}
	
	public Actuacion toBO() {
		Actuacion actuacion = new Actuacion();
		actuacion.setCodiActu(codiActu);
		actuacion.setDescActu(descActu);
		actuacion.setEstaActu(estaActu);
		actuacion.setFechCrea(fechCrea);
		actuacion.setFechVige(fechVige);
		actuacion.setNoabActu(noabActu);
		actuacion.setNombActu(nombActu);
		actuacion.setNoviActu(noviActu);
		actuacion.setTipoActu(tipoActu);
		return actuacion;
	}

}
