package thales.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Estructura de la Respuesta que devuelve el servicio")
public class ApiResponseRestEmployee<T> {

    private String status;
    private String message;
    private Object data;

    public ApiResponseRestEmployee() {
    }

    public ApiResponseRestEmployee(String status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}

