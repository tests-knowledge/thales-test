package thales.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import thales.entities.commons.Actuacion;

@Repository
public interface IActuacionRepository extends JpaRepository<Actuacion, Short>{
	
	@Query(value = "SELECT MAX(CODI_ACTU) + 1 FROM Actuacion", nativeQuery = true)
    public Short getIdConsecutivo();
	
	@Query("SELECT a FROM Actuacion a WHERE esta_actu = :estaActu")
	public List<Actuacion> getListaActuacionesPorEstado(String estaActu);
}
