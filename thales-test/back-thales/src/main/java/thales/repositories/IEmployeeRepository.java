package thales.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import thales.entities.commons.Actuacion;
import thales.entities.commons.Employee;

@Repository
public interface IEmployeeRepository extends JpaRepository<Employee, Short>{
}
