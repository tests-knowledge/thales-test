package thales.services.implementation;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import thales.dto.ApiResponseRestEmployee;
import thales.dto.EmployeeDTO;
import thales.entities.commons.Employee;
import thales.repositories.IEmployeeRepository;
import thales.services.IEmployeeService;

@Service
public class EmployeeServiceImpl extends CommonServiceImpl<Employee, IEmployeeRepository, Short>
		implements IEmployeeService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public List<Employee> getListaEmployees() {
		restTemplate = new RestTemplate();

		System.out.println("getListaEmployees service");

		String url = "http://dummy.restapiexample.com/api/v1/employees";

		@SuppressWarnings("rawtypes")
		ApiResponseRestEmployee data = restTemplate.getForObject(url, ApiResponseRestEmployee.class);

		System.out.println("data.getStatus(): " + data.getStatus());
		System.out.println("data.getMessage(): " + data.getMessage());
		System.out.println("data.getResult(): " + data.getData());

		ObjectMapper mapper = new ObjectMapper();
		Employee[] empleados = mapper.convertValue(data.getData(), Employee[].class);

		System.out.println("empleados.length: " + empleados.length);
		System.out.println("empleados: " + empleados);
		
		return Arrays.asList(empleados);
	}

	@Override
	public Employee getEmployeeById(Short idEmployee) {
		System.out.println("One getListaEmployees service");
		restTemplate = new RestTemplate();

		String url = "http://dummy.restapiexample.com/api/v1/employee/" + idEmployee;

		System.out.println("url: " + url);

		@SuppressWarnings("rawtypes")
		ApiResponseRestEmployee data = restTemplate.getForObject(url, ApiResponseRestEmployee.class);

		System.out.println("data.getStatus(): " + data.getStatus());
		System.out.println("data.getMessage(): " + data.getMessage());
		System.out.println("data.getResult(): " + data.getData());

		ObjectMapper objectMapper = new ObjectMapper();
	    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    Employee empleados = null;
			empleados = objectMapper.convertValue((data.getData()), Employee.class);
			System.out.println("empleado: "+empleados.toString());

		return empleados;
	}
	
	@Override
	public EmployeeDTO calcAnualSalary(EmployeeDTO employee) {
		employee.setEmployeeAnualSalary(employee.getEmployeeSalary()*12);
		return employee;
	}

	@Override
	public Short getIdConsecutivo() {
		// TODO Auto-generated method stub
		return null;
	}

}
