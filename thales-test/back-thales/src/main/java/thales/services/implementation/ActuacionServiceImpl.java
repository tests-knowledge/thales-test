package thales.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import thales.entities.commons.Actuacion;
import thales.repositories.IActuacionRepository;
import thales.services.IActuacionService;

@Service
public class ActuacionServiceImpl extends CommonServiceImpl<Actuacion, IActuacionRepository, Short> implements IActuacionService{

	@Autowired
	private IActuacionRepository iactuacionRepository;
	
	@Override
	public Short getIdConsecutivo() {
		return iactuacionRepository.getIdConsecutivo();
	}

	@Override
	public List<Actuacion> getListaActuacionesPorEstado(String estaActu) {
		return iactuacionRepository.getListaActuacionesPorEstado(estaActu);
	}

}
