package thales.services;

import java.util.List;
import java.util.Optional;

import thales.dto.EmployeeDTO;
import thales.entities.commons.Employee;

public interface IEmployeeService extends CommonService<Employee, Short>{

public Short getIdConsecutivo();
	
	List<Employee> getListaEmployees();
	
	Employee getEmployeeById(Short idEmployee);
	
	EmployeeDTO calcAnualSalary(EmployeeDTO emp);
}
