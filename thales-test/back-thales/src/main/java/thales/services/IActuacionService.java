package thales.services;

import java.util.List;

import thales.entities.commons.Actuacion;

public interface IActuacionService extends CommonService<Actuacion, Short>{

public Short getIdConsecutivo();
	
	List<Actuacion> getListaActuacionesPorEstado(String estaActu);
}
