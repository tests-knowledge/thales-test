package thales.excepciones;

public class ResponseApiException extends Exception{
	
	private static final long serialVersionUID = 9114160761948518513L;
	
	private final Integer code;
	 
    public ResponseApiException(Integer code) {
        super();
        this.code = code;
    }
    
    public ResponseApiException(String message, Throwable cause, Integer code) {
        super(message, cause);
        this.code = code;
    }
    
    public ResponseApiException(String message, Integer code) {
        super(message);
        this.code = code;
    }
    
    public ResponseApiException(Throwable cause, Integer code) {
        super(cause);
        this.code = code;
    }
    
    public ResponseApiException(String message) {
    	super(message);
        this.code = null;		
    }
    
    public Integer getCode() {
        return this.code;
    }
}
