package thales.entities.commons;

import java.io.Serializable;
import javax.persistence.*;

import thales.dto.ActuacionDTO;

import java.util.Date;


/**
 * The persistent class for the "actuacion" database table.
 * 
 */
@Entity
@Table(name="actuacion")
@NamedQuery(name="Actuacion.findAll", query="SELECT a FROM Actuacion a")
public class Actuacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="codi_actu", unique=true, nullable=false)
	private Short codiActu;

	@Column(name="desc_actu", length=1000)
	private String descActu;

	@Column(name="esta_actu", nullable=false, length=2)
	private String estaActu;

	@Temporal(TemporalType.DATE)
	@Column(name="fech_crea", nullable=false)
	private Date fechCrea;

	@Temporal(TemporalType.DATE)
	@Column(name="fech_vige")
	private Date fechVige;

	@Column(name="noab_actu", length=20)
	private String noabActu;

	@Column(name="nomb_actu", nullable=false, length=100)
	private String nombActu;

	@Column(name="novi_actu", length=100)
	private String noviActu;

	@Column(name="tipo_actu", nullable=false, length=2)
	private String tipoActu;

	public Actuacion() {
		
	}
	public Actuacion(Short codiActu, String descActu, String estaActu, Date fechCrea, Date fechVige, String noabActu,
			String nombActu, String noviActu, String tipoActu) {
		this.codiActu = codiActu;
		this.descActu = descActu;
		this.estaActu = estaActu;
		this.fechCrea = fechCrea;
		this.fechVige = fechVige;
		this.noabActu = noabActu;
		this.nombActu = nombActu;
		this.noviActu = noviActu;
		this.tipoActu = tipoActu;
	}

	public Short getCodiActu() {
		return this.codiActu;
	}

	public void setCodiActu(Short codiActu) {
		this.codiActu = codiActu;
	}

	public String getDescActu() {
		return this.descActu;
	}

	public void setDescActu(String descActu) {
		this.descActu = descActu;
	}

	public String getEstaActu() {
		return this.estaActu;
	}

	public void setEstaActu(String estaActu) {
		this.estaActu = estaActu;
	}

	public Date getFechCrea() {
		return this.fechCrea;
	}

	public void setFechCrea(Date fechCrea) {
		this.fechCrea = fechCrea;
	}

	public Date getFechVige() {
		return this.fechVige;
	}

	public void setFechVige(Date fechVige) {
		this.fechVige = fechVige;
	}

	public String getNoabActu() {
		return this.noabActu;
	}

	public void setNoabActu(String noabActu) {
		this.noabActu = noabActu;
	}

	public String getNombActu() {
		return this.nombActu;
	}

	public void setNombActu(String nombActu) {
		this.nombActu = nombActu;
	}

	public String getNoviActu() {
		return this.noviActu;
	}

	public void setNoviActu(String noviActu) {
		this.noviActu = noviActu;
	}

	public String getTipoActu() {
		return this.tipoActu;
	}

	public void setTipoActu(String tipoActu) {
		this.tipoActu = tipoActu;
	}
	
	public ActuacionDTO toBO() {
		ActuacionDTO actuacionDTO = new ActuacionDTO();
		actuacionDTO.setCodiActu(codiActu);
		actuacionDTO.setDescActu(descActu);
		actuacionDTO.setEstaActu(estaActu);
		actuacionDTO.setFechCrea(fechCrea);
		actuacionDTO.setFechVige(fechVige);
		actuacionDTO.setNoabActu(noabActu);
		actuacionDTO.setNombActu(nombActu);
		actuacionDTO.setNoviActu(noviActu);
		actuacionDTO.setTipoActu(tipoActu);
		return actuacionDTO;
	}

}