package thales.entities.commons;

import java.io.Serializable;

import javax.persistence.*;
import thales.dto.EmployeeDTO;
import java.util.Objects;


/**
 * The persistent class for the "actuacion" database table.
 * 
 */
@Entity
@Table(name="employee")
//@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique=true, nullable=false)
	private Short id;

	@Column(name="employee_name", nullable=false, length=1000)
	private String employee_name;

	@Column(name="employee_salary", nullable=false)
	private float employee_salary;

	@Column(name="employee_age", nullable=false)
	private Integer employee_age;

	@Column(name="profile_image", length=1000)
	private String profile_image;
    

	public Employee() {
		
	}
	
	
	public Employee(Short id, String employee_name, float employee_salary, Integer employee_age, String profile_image) {
		super();
		this.id = id;
		this.employee_name = employee_name;
		this.employee_salary = employee_salary;
		this.employee_age = employee_age;
		this.profile_image= profile_image;
	}
	
	
	
	public Short getId() {
		return id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getemployee_name() {
		return employee_name;
	}


	public void setemployee_name(String employee_name) {
		this.employee_name = employee_name;
	}


	public float getemployee_salary() {
		return employee_salary;
	}


	public void setemployee_salary(float employee_salary) {
		this.employee_salary = employee_salary;
	}


	public Integer getemployee_age() {
		return employee_age;
	}


	public void setemployee_age(Integer employee_age) {
		this.employee_age = employee_age;
	}
	

	public String getprofile_image() {
		return profile_image;
	}


	public void setprofile_image(String profile_image) {
		this.profile_image = profile_image;
	}


	public EmployeeDTO toBO() {
		EmployeeDTO employeeDTO = new EmployeeDTO();
		employeeDTO.setIdEmployee(id);
		employeeDTO.setEmployeeName(employee_name);
		employeeDTO.setEmployeeSalary(employee_salary);
		employeeDTO.setEmployeeAge(employee_age);
		employeeDTO.setProfileImage(profile_image);
		return employeeDTO;
	}


	@Override
	public int hashCode() {
		return Objects.hash(employee_age, employee_name, employee_salary, id, profile_image);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		return Objects.equals(employee_age, other.employee_age) && Objects.equals(employee_name, other.employee_name)
				&& Objects.equals(employee_salary, other.employee_salary) && Objects.equals(id, other.id)
				&& Objects.equals(profile_image, other.profile_image);
	}


	@Override
	public String toString() {
		return "Employee [idEmployee=" + id + ", employee_name=" + employee_name + ", employee_salary="
				+ employee_salary + ", employee_age=" + employee_age + ", profile_image=" + profile_image + "]";
	}
	
	

}