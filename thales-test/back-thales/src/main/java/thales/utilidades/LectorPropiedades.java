package thales.utilidades;

import java.util.ResourceBundle;

public class LectorPropiedades {
	 public static String obtenerValorPropiedad(final String archivo, final String clave) {
        String valor = "";        
        try {
        	final ResourceBundle propiedades = ResourceBundle.getBundle(archivo);
            valor = propiedades.getString(clave);
        } catch (Exception ee) {
            valor = "";
        }
        return valor;
    }
}
