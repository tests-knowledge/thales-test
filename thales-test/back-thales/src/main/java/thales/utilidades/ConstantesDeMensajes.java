package thales.utilidades;

public class ConstantesDeMensajes {

	private ConstantesDeMensajes() {
		
	}

	public static final String INFORMACION_ENCONTRADA = "Se encontró con éxito la información.";
	public static final String INFORMACION_NO_ENCONTRADA = "No se ha encontrado información.";
	public static final String ERROR_PETICION = "Se ha producido un error en la petición.";
	public static final String LISTA_CARGADA = "Se cargo la información con éxito.";
	public static final String ELIMINADO_EXITOSO = "Se ha eliminado con éxito la información.";
	public static final String GUARDADO_EXITOSO = "Se ha guardado con éxito la información.";
	public static final String ACTUALIZADO_EXITOSO = "Se actualizo con éxito la información.";
}
