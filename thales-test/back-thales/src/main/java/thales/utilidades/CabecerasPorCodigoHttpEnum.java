package thales.utilidades;

import java.util.EnumMap;

import org.springframework.http.HttpStatus;

public enum CabecerasPorCodigoHttpEnum {
	NO_AUTORIZADO(HttpStatus.UNAUTHORIZED, "Usuario no autorizado."),
	SIN_PERMISOS(HttpStatus.FORBIDDEN, "El usuario no tiene permisos para realizar esta operación."),
	ERROR_INTERNO(HttpStatus.INTERNAL_SERVER_ERROR,
			"Ha ocurrdo un error, por favor intente mas tarde. Si el error persiste comuniquese con soporte técnico."),
	NO_ENCONTRADO(HttpStatus.NOT_FOUND, "No se encontro la informacion solicitada"),
	TIPO_DATO_NO_SOPORTADO(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Tipo de archivo no permitido"),
	ERROR_REQUEST(HttpStatus.BAD_REQUEST, "Error en el request"),
	NO_IMPLEMENTADO(HttpStatus.NOT_IMPLEMENTED, "Servicio no implementado"),
	CONTENIDO_NO_ENCONTRADO(HttpStatus.NO_CONTENT, "No se encontro informacion para la consulta ingresada");

	public HttpStatus getCodigoError() {
		return codigoError;
	}

	private static final EnumMap<HttpStatus, CabecerasPorCodigoHttpEnum> MAP_VALOR = new EnumMap<>(HttpStatus.class);

	static {
		for (CabecerasPorCodigoHttpEnum mensaje : values()) {
			MAP_VALOR.put(mensaje.codigoError, mensaje);
		}
	}

	private final HttpStatus codigoError;
	private final String mensaje;

	/**
	 * @param nombre
	 * @param valor
	 * @param descripcion
	 */
	private CabecerasPorCodigoHttpEnum(HttpStatus codigoError, String mensaje) {
		this.codigoError = codigoError;
		this.mensaje = mensaje;
	}

	public static CabecerasPorCodigoHttpEnum buscarPorValor(HttpStatus codigoError) {
		return MAP_VALOR.get(codigoError);
	}

	public String getMensaje() {
		return mensaje;
	}
}
