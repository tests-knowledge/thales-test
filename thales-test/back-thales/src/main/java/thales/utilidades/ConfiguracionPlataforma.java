package thales.utilidades;

public class ConfiguracionPlataforma {

	public ConfiguracionPlataforma() {
		
	}
	
	public static final String DOCROOT = System.getProperty("PATH-DOCUMENTOS");
	
	public static final String ARCHIVOS_PLANTILLAS = DOCROOT + "plantillas/cartas/";
	public static final String TEMPORALES_CARTAS_EDITADAS = DOCROOT + "cartas/cartas-word/";
	public static final String DOCARCHIVOSCARTAS = DOCROOT + "archivos/cartas/";
	public static final String DOCARCHIVOSASIG = DOCROOT + "archivos/asignaciones/";
}
