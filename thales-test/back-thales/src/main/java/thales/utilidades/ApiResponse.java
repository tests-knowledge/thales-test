package thales.utilidades;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Estructura de la Respuesta que devuelve el servicio")
public class ApiResponse<T> {

    private int status;
    private String message;
    private Object data;

    public ApiResponse() {
    }

    public ApiResponse(int status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ApiResponse(int status, String message, Object data, String idUsuario) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getdata() {
        return data;
    }

    public void setdata(Object data) {
        this.data = data;
    }

}

