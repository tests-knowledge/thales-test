package thales.utilidades;

public class Constantes {
	private Constantes() {

	}
	
	public static final String ESTADO_REGISTRO_ACTIVO = "AC";
	public static final String ESTADO_REGISTRO_INACTIVO = "IN";
	
}
