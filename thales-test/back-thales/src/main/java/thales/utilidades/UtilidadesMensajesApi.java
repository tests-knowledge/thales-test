package thales.utilidades;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/UtilidadesMensajesApi")
public class UtilidadesMensajesApi {
	public static final Logger logger = LoggerFactory.getLogger(UtilidadesMensajesApi.class);
	
    public String obtenerMensajeDeApi(String nombrePropiedad) {
        String mensaje;
        mensaje = LectorPropiedades.obtenerValorPropiedad("MensajesApi", nombrePropiedad);
        if (mensaje == null || mensaje.isEmpty()) {
            logger.error("En el archivo MensajesApi.properties no se ha encontrado ningún valor para la propiedad " + nombrePropiedad);
        }
        return mensaje;
    }
}
