package thales;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import thales.dto.EmployeeDTO;
import thales.entities.commons.Employee;
import thales.services.IEmployeeService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

@SpringBootTest
class ThalesApplicationTests {

	@Autowired
	private IEmployeeService iemployeeService;
	
	void contextLoads() {
	}
	
	@SuppressWarnings({ "deprecation", "unused" })
	@Test
    @Order(1)
    void firstOne() {
		EmployeeDTO employeeDto = new EmployeeDTO();
		employeeDto.setIdEmployee((short) 27);
		employeeDto.setEmployeeName("Gabriel Reyes");
		employeeDto.setEmployeeAge(28);
		employeeDto.setEmployeeSalary(6000);
		employeeDto = iemployeeService.calcAnualSalary(employeeDto);
		assertEquals(72000, employeeDto.getEmployeeAnualSalary());
		assertNull(employeeDto);
		assertFalse(employeeDto.getEmployeeAnualSalary() == 8000);
    }
	
	@SuppressWarnings({ "unused" })
	@Test
    @Order(2)
    void secondOne() {
		Employee employee = new Employee();
		employee = iemployeeService.getEmployeeById((short) 1);
		assertEquals("Tiger Nixon", employee.getemployee_name());
		assertNull(employee);
		assertFalse(employee.getId() == 2);
    }
	
	@Test
    @Order(3)
    void thirdOne() {
		List<Employee> employees = new ArrayList<Employee>();
		employees = iemployeeService.getListaEmployees();
		assertNull(employees);
    }

}
