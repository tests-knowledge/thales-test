import moment from 'moment-business-days';
import holiday from 'colombia-holidays';

const holidayColombia = holiday.getColombiaHolidaysByYear(moment().format('YYYY'));
const array = [];
// eslint-disable-next-line array-callback-return
holidayColombia.map((val) => { array.push(val.holiday); });

// eslint-disable-next-line import/prefer-default-export
export const calculateDays = (fecha = '2020-01-01') => {
  moment.updateLocale('us', {
    workingWeekdays: [1, 2, 3, 4, 5],
    holidays: array,
    holidayFormat: 'YYYY-MM-DD',
  });
  const hoy = moment().format('YYYY-MM-DD');
  const val = moment(fecha, 'YYYY-MM-DD').businessDiff(moment(hoy, 'YYYY-MM-DD'));
  return val;
};
