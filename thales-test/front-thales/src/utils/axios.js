/* eslint-disable no-undef */
import axios from 'axios'
const dotenv = require('dotenv');
dotenv.config();

const instance = axios.create({
  baseURL: process.env.REACT_APP_SERVER_URL
});



export default instance;
