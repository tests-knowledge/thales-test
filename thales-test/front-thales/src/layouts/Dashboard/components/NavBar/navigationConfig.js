/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import { useTranslation } from 'react-i18next';

const NavigationConfig = () => {
  const { t } = useTranslation();
  let pages = [
    {
      title: t('sidebar.pages.overview'),
      href: '/overview',
      icon: HomeIcon
    },
  ];
  pages.push(
    {
      title: 'Empleados',
      href: '/employees',
      icon: AssignmentIndIcon,
      children: [
        {
          title: 'Employees List',
          href: '/employees/list'
        },
      ]
    },
  )
  return [
    {
      title: t('sidebar.pages.title'),
      pages: pages.length > 0 ? pages: 
        {
          title: t('sidebar.pages.overview'),
          href: '/overview',
          icon: HomeIcon
        },
    }
  ];
};

export default NavigationConfig;
