import React, { Suspense, useState, useEffect } from 'react';
import { renderRoutes } from 'react-router-config';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { LinearProgress } from '@material-ui/core';

import { NavBar, TopBar } from './components';

import Cookies from 'js-cookie';

let socket;

const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden'
  },
  topBar: {
    zIndex: 2,
    position: 'relative'
  },
  container: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden'
  },
  navBar: {
    zIndex: 3,
    width: 256,
    minWidth: 256,
    flex: '0 0 auto'
  },
  content: {
    overflowY: 'auto',
    flex: '1 1 auto'
  }
}));

const Dashboard = props => {
  const { route } = props;

  const classes = useStyles();
  const [openNavBarMobile, setOpenNavBarMobile] = useState(false);

  // const [name, setName] = useState('');
  // const [room, setRoom] = useState('');
  // const [users, setUsers] = useState('');
  const [notification, setNotification] = useState('');
  const [notifications, setNotifications] = useState([]);
  const roomId = Cookies.get('identification');
  const nameId = Cookies.get('firstName');

  useEffect(() => {
    if (Cookies.get('roles') === undefined || localStorage.getItem('jwt-token') === null || JSON.parse(Cookies.get('roles')).length === 0){
      window.location.href = '/auth/login';
    }
  }, []);
  
  useEffect(() => {

  }, []);

  const handleNavBarMobileOpen = () => {
    setOpenNavBarMobile(true);
  };

  const handleNavBarMobileClose = () => {
    setOpenNavBarMobile(false);
  };

  return (
    <div className={classes.root}>
      {localStorage.getItem('jwt-token') !== null && (
        <>
          <TopBar
            className={classes.topBar}
            name={nameId}
            notification={notification}
            notifications={notifications}
            onOpenNavBarMobile={handleNavBarMobileOpen}
            room={roomId}
            setNotification={setNotification}
            setNotifications={setNotifications}
            socket={socket}
          />
          <div className={classes.container}>
            <NavBar
              className={classes.navBar}
              onMobileClose={handleNavBarMobileClose}
              openMobile={openNavBarMobile}
            />
            <main className={classes.content}>
              <Suspense fallback={<LinearProgress />}>
                {renderRoutes(route.routes)}
              </Suspense>
            </main>
          </div>
        </>
      )}
      {/* <ChatBar /> */}
    </div>
  );
};

Dashboard.propTypes = {
  route: PropTypes.object
};

export default Dashboard;
