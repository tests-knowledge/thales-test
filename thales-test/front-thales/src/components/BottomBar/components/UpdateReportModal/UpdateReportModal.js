import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  LinearProgress,
  TextField,
  Typography,
  colors,
  Grid,
  InputAdornment,

} from '@material-ui/core';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import moment from 'moment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import EventIcon from '@material-ui/icons/Event';
import RateReviewIcon from '@material-ui/icons/RateReview';
import { ProjectCard } from './components';
import { Paginate } from 'components';
import Cookies from 'js-cookie';
import { useHistory } from 'react-router';


const useStyles = makeStyles(theme => ({
  root: {
    width: 960
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    // maxWidth: 720,
    marginBottom: '15px'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  projects: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  loader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  applyButton: {
    width: '100%',
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  formUpdateReport: {
    alignSelf: 'center',
  },
  resultsUpdateReport: {
    alignSelf: 'baseline',
  },
  results: {
    marginTop: theme.spacing(3)
  },
  paginate: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'center'
  }
}));

const UpdateReportModal = props => {
  const { projects, open, onClose, onApply, className, ...rest } = props;
  const email = Cookies.get('email');
  const nombre = Cookies.get('firstName');
  const apellido = Cookies.get('lastName');
  const identificacion = Cookies.get('identification');
  const telefono = Cookies.get('phone');
  const history = useHistory();
  const [executors, setExecutors] = useState({
    correo: {
      nombre: '',
      cliente: '',
      entregaServicio: '',
      observaciones: '',
      contacto: { email, nombre, telefono },
    },
  });
  // eslint-disable-next-line no-unused-vars
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = useState(1);
  const [charge, setCharge] = useState({
    show: false,
    quantity: 0,
  });
  // eslint-disable-next-line no-unused-vars
  const [totalReg, setTotalReg] = useState(projects.length);
  const [projectsSelected, setProjectsSelected] = useState(projects.length > rowsPerPage ? projects.slice(0,rowsPerPage) : projects);
  const classes = useStyles();

  // const handleChange = event => {
  //   event.persist();
  //   setValue(event.target.value);
  // };

  const handleChange = (e) => {
    const { target } = e;
    setExecutors({
      ...executors,
      correo: { ...executors.correo, [target.name]: target.value },
    });
  };




  const handleChangePage = (e) => {

    setProjectsSelected(
      projects.slice(
        rowsPerPage * (e.selected),
        (totalReg > rowsPerPage * (e.selected) ? 
          (rowsPerPage * (e.selected) + rowsPerPage) :
          (totalReg - rowsPerPage * (e.selected)))
      )
    );
    setPage(e.selected + 1);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setCharge({
      ...charge,
      show:true,
    })
    try {
      let pregress = 0;
      for (let i = 0; i < projects.length; i++) {
        const petition = await axios.put(`projects/${projects[i]._id}`,
          {
            field: '_id',
            '$push': {
              reporteActualizacion: {
                identificacion,
                usuario: `${nombre} ${apellido}`,
                fechaReporte:  moment().format('YYYY-MM-DD HH:mm')
              }
            },
          } 
          ,getAuthorization());
        if (petition.status === 200) {
          pregress = pregress + (100/projects.length);
          setCharge({
            ...charge,
            show: true,
            quantity: pregress,
          })
        }
      }

      if (pregress == 100) {
        const emailConsult = await axios.post('email/hitos',
          {
            otps: projects,
            correo: executors.correo,
          } 
          ,getAuthorization());
        if (emailConsult.status == 201) {
          history.go(0);
        }
      }
        

    } catch (error) {
      console.error(error);
    }      
  };


  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        {...rest}
      >
        <DialogTitle>
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
          >
            Reporte Actualización
          </Typography>
        </DialogTitle>
        <DialogContent className={classes.content}>
          <Grid
            container
            spacing={2}
          >
            <Grid
              className={classes.formUpdateReport}
              item
              md={6}
              sm={12}
              xs={12}
            >
              <div className={classes.results}>
                {/* cabecera */}
                <div>
                  <Typography
                    color="textSecondary"
                    gutterBottom
                    variant="body2"
                  >
                    {totalReg} Proyectos seleccionados. Página {page} de{' '}
                    {Math.ceil(totalReg / rowsPerPage)}
                  </Typography>
                </div>
                {/* cuerpo */}
                <div>
                  {projectsSelected.map(project => (
                    <ProjectCard
                      key={project.id}
                      project={project}
                    />
                  ))}
                  <div className={classes.paginate}>
                    <Paginate
                      onPageChange={handleChangePage}
                      pageCount={Math.ceil(totalReg / rowsPerPage)}
                    />
                  </div>
                </div>
              </div>
            </Grid>
            { projects && (
              <Grid
                className={classes.formUpdateReport}
                item
                md={6}
                sm={12}
                xs={12}
              >
                <form onSubmit={handleSubmit}>
                  <Grid
                    container
                    spacing={2}
                  >
                    <Grid
                      item
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <TextField
                        InputProps={{ startAdornment: <InputAdornment position="start"><AccountCircleIcon /></InputAdornment> }}
                        fullWidth
                        id="nombre"
                        label="Señor(a)"
                        name="nombre"
                        onChange={handleChange}
                        required
                        value={executors?.correo?.nombre || ''}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <TextField
                        InputProps={{ startAdornment: <InputAdornment position="start"><ContactMailIcon /></InputAdornment> }}
                        fullWidth
                        id="cliente"
                        label="Nombre del cliente"
                        name="cliente"
                        onChange={handleChange}
                        required
                        value={executors?.correo?.cliente || ''}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <TextField
                        InputProps={{ startAdornment: <InputAdornment position="start"><EventIcon /></InputAdornment> }}
                        fullWidth
                        id="entregaServicio"
                        label="Entrega del servicio"
                        name="entregaServicio"
                        onChange={handleChange}
                        required
                        type="date"
                        value={executors?.correo?.entregaServicio || ''}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      <TextField
                        InputProps={{ startAdornment: <InputAdornment position="start"><RateReviewIcon /></InputAdornment> }}
                        fullWidth
                        id="observaciones"
                        label="Observaciones"
                        name="observaciones"
                        onChange={handleChange}
                        required
                        value={executors?.correo?.observaciones || ''}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid
                      item
                      md={12}
                      sm={12}
                      xs={12}
                    >
                      {charge.show && (
                        <LinearProgress
                          value={charge.quantity}
                          variant="determinate"
                        />
                      )}
                      <Button
                        className={classes.applyButton}
                        type="submit"
                        variant="contained"
                      >
                        Enviar
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </Grid>
            )}
          </Grid>
        </DialogContent>
      </div>
    </Dialog>
  );
};

UpdateReportModal.propTypes = {
  projects: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

export default UpdateReportModal;
