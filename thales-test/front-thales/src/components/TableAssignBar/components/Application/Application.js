import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Button,
  Card,
  CircularProgress,
  CardActions,
  CardContent,
  colors,
  Dialog,
  DialogContent,
  LinearProgress,
  TextField,
  Typography,
} from '@material-ui/core';
import moment from 'moment';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import { useHistory } from 'react-router';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// import getInitials from 'utils/getInitials';

const useStyles = makeStyles(theme => ({
  dialog: {
    display: 'flex',
    flexFlow: 'column',
    width: '100%',
    height: '100%',
    justifyContent: 'space-between',
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between'
  },
  loader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  cancelButton: {
    color: theme.palette.white,
    backgroundColor: colors.red[600],
    '&:hover': {
      backgroundColor: colors.red[900]
    }
  },
  card: {
    minWidth: '250px',
    marginBottom: '20px',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'space-between',
  },
  cardContainer: {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'space-around',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  conainer:  {
    minWidth: 'calc(50% + 150px)',
  }
}));

const Application = props => {
  const { author, className, data, fetchUsers, open, onClose, onApply, seldected, ...rest } = props;
  
  const history = useHistory()
  const [value, setValue] = useState({});
  const [executors, setExecutors] = useState({});
  const [progress, setProgress] = useState({ cuantity: 0, show:false });
  // eslint-disable-next-line no-unused-vars
  const [error, setError] = useState('');
  const classes = useStyles();

  const handleChange = (event) => {
    event.persist();
    const names = event.target.name.split('#');
    const dataAreas = value[names[0]] && value[names[0]][names[1]] ? { ...value[names[0]][names[1]] }: '';
    setValue({ ...value, [names[0]]: {
      ...value[names[0]],
      [names[1]]: {
        ...dataAreas,
        [names[2]]: event.target.value,
      }
    } });
  };

  const handleAssign = async (e) => {
    try {
      setProgress({...progress, show: true});
      e.preventDefault();

      let acumulate = 0;

      const groups = Object.keys(data) || [];
      // eslint-disable-next-line no-unused-vars
      const queryList =  [];
      for (let i = 0; i < groups.length; i++) {
        for (let j = 0; j < Object.keys(data[groups[i]]).length; j++) {
          const area = Object.keys(data[groups[i]])[j];
          for (let l = 0; l < Object.keys(data[groups[i]][area]).length; l++) {
            const role = Object.keys(data[groups[i]][area])[l];
            for (let t = 0; t < Object.keys(data[groups[i]][area][role]).length; t++) {
              const user = value[groups[i]][area][role].split('#');
              data[groups[i]][area][role][t].logs.push({
                status: 'Asignación#AS',
                description: `Tarea asignada a ${role} ${user[0]} othId: ${data[groups[i]][area][role].projectId}`,
                createdAt: moment().format('YYYY-MM-DD HH:mm'),
                author: value[groups[i]][area][role],
              });

              const petition = await axios.put(`/tasks/${data[groups[i]][area][role][t]._id}`,
                {
                  field: '_id',
                  role,
                  author: value[groups[i]][area][role],
                  message: `Tarea asignada a ${user[0]}`,
                  logs: data[groups[i]][area][role][t].logs,
                },
                getAuthorization());
              if (petition.status === 200) {
                acumulate = acumulate + (100/seldected.length);
                setProgress({show: true, cuantity: acumulate});
              }    
            }
          }
        }        
      }
      
      acumulate >= 100 && setTimeout(function(){ history.go(0) }, 1000);
    } catch (error) {
      console.error(error);
    }
  };


  useEffect(() => {
    const fetchData = async () => {
      try {
        const petition = (open === true) ? (await fetchUsers() || []) : [];
        const groups = Object.keys(data) || [];
        let finalList = {};

        for (let i = 0; i < groups.length; i++) {
          for (let j = 0; j < Object.keys(data[groups[i]]).length; j++) {
            const area = Object.keys(data[groups[i]])[j];
            for (let l = 0; l < Object.keys(data[groups[i]][area]).length; l++) {
              const role = Object.keys(data[groups[i]][area])[l];
              const usuarios = petition.filter(arr => arr.roles.find(item => item.group === groups[i] && item.area === area && role ));
              const areaData = finalList[groups[i]] ? { ...finalList[groups[i]] } : {}; 
              const rolData = finalList[groups[i]] && finalList[groups[i]][area] ? { ...finalList[groups[i]][area] } : {}; 
              
              finalList = {
                ...finalList,
                [groups[i]]: {
                  ...areaData,
                  [area]: {
                    ...rolData,
                    [role]: usuarios,
                  }
                } 
              };   
            }
          }        
        }
        // finalList = { ...finalList, [roles[i]]: usuarios };  
        setExecutors(finalList);
      } catch (error) {
        console.error(error)
      }
      
    };

    fetchData();
    if (open === false) {
      setValue({});
      setExecutors({});
      setProgress({show: false, cuantity: 0});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  return (
    <Dialog
      classes={{ paper: classes.conainer }}
      fullScreen
      // fullWidth={true}
      maxWidth="xl"
      onClose={onClose}
      open={open}
    >
      <DialogContent
        {...rest}
      >
        <form
          onSubmit={handleAssign}
          style={{ height: '100%' }}
        >
          <div className={classes.dialog}>
            {/* contenido */}
            <div>
              <div className={classes.header} >
                <Typography
                  align="center"
                  gutterBottom
                  variant="h3"
                >
                  Asignar Tarea
                  {seldected.length > 1 && 's'}
                </Typography>
                <Typography
                  align="center"
                  className={classes.subtitle}
                  variant="subtitle2"
                >
                  Selecciona un usuario de las listas.
                </Typography>
              </div>

              {data ? Object.keys(data).map((group, index) =>
                <div>
                  <Typography
                    className={classes.title}
                    color="textSecondary"
                    gutterBottom
                  >
                    Grupo:
                  </Typography>
                  <Typography
                    component="h2"
                    variant="h5"
                  >
                    {group}
                  </Typography>

                  {data ? Object.keys(data[group]).map((area, number) =>
                    <Accordion>
                      <AccordionSummary
                        aria-controls="panel1a-content"
                        expandIcon={<ExpandMoreIcon />}
                        id="panel1a-header"
                      >
                        <Typography className={classes.heading}>{area}</Typography>
                      </AccordionSummary>
                      <AccordionDetails className={classes.cardContainer}>
                        {/* <div className={classes.cardContainer} > */}
                        {data ? Object.keys(data[group][area]).map( (role, index) =>
                          <Card className={classes.card}>
                            <CardContent>
                              <Typography
                                className={classes.title}
                                color="textSecondary"
                                gutterBottom
                              >
                                  Asignacion para el rol:
                              </Typography>
                              <Typography
                                component="h2"
                                variant="h5"
                              >
                                {role}
                              </Typography>
                              <Typography
                                className={classes.pos}
                                color="textSecondary"
                              >
                                {`${data[group][area] ? data[group][area][role].length : 0} Tarea${(data[group][area] ? data[group][area][role].length : 0) > 1 ? 's' : ''}`}
                              </Typography>
                              {data[group][area] ? data[group][area][role].map(arr => 
                                <Typography
                                  component="p"
                                  variant="body2"
                                >
                                  {arr.activityId}
                                </Typography>
                              ) : ''}
                            </CardContent>
                            <CardActions>
                              <TextField
                                SelectProps={{ native: true }}
                                defaultValue={''}
                                disabled={progress.show}
                                fullWidth
                                label={role}
                                name={`${group}#${area}#${role}`}
                                onChange={event =>
                                  handleChange(event)
                                }
                                required
                                select
                                style={{marginTop: 25}}
                                value={(value[group] && value[group][area] && value[group][area][role]) || ''}
                                variant="outlined"
                              >
                                <option
                                  disabled
                                  value=""
                                />
                                {executors[group] && executors[group][area] ?  executors[group][area][role].map(option => (
                                  <option
                                    key={option.identification}
                                    value={`${option.firstName} ${option.lastName}#${option.identification}`}
                                  >
                                    {`${option.firstName} ${option.lastName}`}
                                  </option>
                                )) : ''}
                              </TextField>
                            </CardActions>
                          </Card>
                        ) : <CircularProgress />}
                        {/* </div> */}
                      </AccordionDetails>
                    </Accordion>
                  ) : ''}
                </div>
              ) : ''}
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
              >
                {error || ''}
              </Typography>
              {progress.show && (
                <LinearProgress
                  value={progress.cuantity}
                  variant="determinate"
                />
              )}
            </div>
            {/* acciones */}
            <div className={classes.actions}>
              <Button
                className={classes.applyButton}
                type="submit"
                variant="contained"
              >
                Asignar
              </Button>
              <Button
                className={classes.cancelButton}
                onClick={onClose}
                variant="contained"
              >
                Cancelar
              </Button>
            </div>
          </div>
        </form>
      </DialogContent>
    </Dialog>
  );
};

Application.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  fetchUsers: PropTypes.func,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

export default Application;
