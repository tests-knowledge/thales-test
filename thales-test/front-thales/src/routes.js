/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';

import AuthLayout from './layouts/Auth';
import ErrorLayout from './layouts/Error';
import DashboardLayout from './layouts/Dashboard';
import OverviewView from './views/Overview';

const routes = (isLogged) => {
  return [
    {
      path: '/',
      exact: true,
      component: isLogged ?  (() => <Redirect to="/overview" />) : (() => <Redirect to="/auth/login" />)
    },
    {
      path: '/auth',
      component: AuthLayout,
      routes: [
        {
          path: '/auth/login',
          exact: true,
          component: lazy(() => import('views/Login'))
        },
        {
          path: '/auth/register',
          exact: true,
          component: lazy(() => import('views/Register'))
        },
        {
          component: () => <Redirect to="/errors/error-404" />
        }
      ]
    },
    {
      path: '/errors',
      component: ErrorLayout,
      routes: [
        {
          path: '/errors/error-401',
          exact: true,
          component: lazy(() => import('views/Error401'))
        },
        {
          path: '/errors/error-404',
          exact: true,
          component: lazy(() => import('views/Error404'))
        },
        {
          path: '/errors/error-500',
          exact: true,
          component: lazy(() => import('views/Error500'))
        },
        {
          component: () => <Redirect to="/errors/error-404" />
        }
      ]
    },
    {
      route: '*',
      component: DashboardLayout,
      routes: [
        {
          path: '/overview',
          exact: true,
          component: OverviewView
        },
        {
          path: '/employees',
          exact: true,
          component: lazy(() => import('views/TaskManagementList'))
        },
        {
          path: '/employees/list',
          exact: true,
          component: lazy(() => import('views/EmployeesList'))
        },
        {
          component: () => <Redirect to="/errors/error-404" />
        }
      ]
    }
  ];
};

export default routes;
