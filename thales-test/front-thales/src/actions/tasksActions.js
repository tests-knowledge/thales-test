export const SELECTED_TASKS = 'SELECTED_TASKS';
export const TASKS_STATUS = 'TASKS_STATUS';

export const selectedTasks = (payload) => dispatch =>
  dispatch({
    type: SELECTED_TASKS,
    payload
  });

export const tasksStatus = (payload) => dispatch =>
  dispatch({
    type: TASKS_STATUS,
    payload
  });