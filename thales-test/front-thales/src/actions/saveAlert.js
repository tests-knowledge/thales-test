export const SAVE_ALERT = 'SAVE_ALERT';

export const saveFormKickoff = (payload) => dispatch =>
  dispatch({
    type: SAVE_ALERT,
    payload
  });