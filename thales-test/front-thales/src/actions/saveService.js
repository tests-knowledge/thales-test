export const SAVE_SERVICE = 'SAVE_SERVICE';

export const saveService = (payload) => dispatch =>
  dispatch({
    type: SAVE_SERVICE,
    payload
  });