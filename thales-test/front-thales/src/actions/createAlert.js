export const CREATE_ALERT = 'CREATE_ALERT';

export const createAlert = (payload) => dispatch =>
  dispatch({
    type: CREATE_ALERT,
    payload
  });