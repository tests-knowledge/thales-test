import * as actionTypes from 'actions';

const serviceKickoff = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.SAVE_SERVICE: {
      return {
        service: action.payload
      };
    }

    default: {
      return state;
    }
  }
};

export default serviceKickoff;
