/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import validate from 'validate.js';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import { Button, TextField } from '@material-ui/core';
import axios from 'utils/axios';
import Cookies from 'js-cookie';
import useRouter from 'utils/useRouter';
import { login } from 'actions';
import { saveToken } from 'tokenUtils';
import socketIOClient from 'socket.io-client';

const schema = {
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' }
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  fields: {
    margin: theme.spacing(-1),
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      flexGrow: 1,
      margin: theme.spacing(1)
    }
  },
  submitButton: {
    marginTop: theme.spacing(2),
    width: '100%'
  }
}));

const LoginForm = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const router = useRouter();
  const dispatch = useDispatch();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleSubmit = async event => {
    event.preventDefault();
    const payload = { username: formState.values.email, password: formState.values.password };
    console.log('formState.values.email: ', formState.values.email);
    console.log('formState.values.password: ', formState.values.password);
    try {
      // const logIn = await axios.post('/auth/sign-in', {apiKeyToken: process.env.REACT_APP_KEY_TOKEN}, {auth:payload});
      // const { token, user } = logIn.data;
      // const userData = {
      //   first_name: user.firstName,
      //   last_name: user.lastName,
      //   email: user.email,
      //   identification: user.identification,
      //   bio: user.workPosition,
      //   role: 'ADMIN' 
      // };
      const token = '602dfd880d38fb37d0cbe040';
      const userData = {
        first_name: 'Gabriel',
        last_name: 'Reyes',
        email: 'gabrielerm93@gmail.com',
        identification: '1032460616',
        bio: 'Full Stack',
        role: 'Admin' 
      };
      const user = {
        id: 27,
        firstName: 'Gabriel',
        lastName: 'Reyes',
        email: 'gabrielerm93@gmail.com',
        identification: '1032460616',
        phone: '3153244590',
        workPosition: 'Full Stack',
        userOnyx: 'gereyes',
        roles: ['Admin'], 
      };
      const userOnyx = user.roles.find((data) => {
        return (data.role === 'Admin');
      })
      dispatch(login(userData));

      Cookies.set('email', user.email);
      Cookies.set('firstName', user.firstName);
      Cookies.set('lastName', user.lastName);
      Cookies.set('id', user.id);
      Cookies.set('roles', JSON.stringify(user.roles));
      Cookies.set('identification', user.identification);
      Cookies.set('phone', user.phone);
      Cookies.set('workPosition', user.workPosition);
      Cookies.set('userOnyx', userOnyx?.userOnyx);
      saveToken(token);
      const isLogin = 'login';
      const userName = `${user.firstName} ${user.lastName}`;
      let roomId = ['mainRoom', user.identification];
      const idUser = user.identification;

      router.history.push('/overview');
    } catch (error) {
      const status = error?.response?.status;
      if (status === 401) {
        setFormState({
          ...formState,
          errors: {
            email: ['usuario y/o contraseña incorrecto'],
            password: ['']
          }
        });
        console.error('usuario y/o contraseña incorrecto')
      } else {
        setFormState({
          ...formState,
          errors: {
            email: ['usuario y/o contraseña incorrecto'],
            password: ['']
          }
        });
        console.error(error);
      }
    }
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <form
      {...rest}
      className={clsx(classes.root, className)}
      onSubmit={handleSubmit}
    >
      <div className={classes.fields}>
        <TextField
          error={hasError('email')}
          fullWidth
          helperText={hasError('email') ? formState.errors.email[0] : null}
          label="Dirección de correo"
          name="email"
          onChange={handleChange}
          value={formState.values.email || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('password')}
          fullWidth
          helperText={
            hasError('password') ? formState.errors.password[0] : null
          }
          label="Contraseña"
          name="password"
          onChange={handleChange}
          type="password"
          value={formState.values.password || ''}
          variant="outlined"
        />
      </div>
      <Button
        className={classes.submitButton}
        color="secondary"
        disabled={!formState.isValid}
        size="large"
        type="submit"
        variant="contained"
      >
        Ingresar
      </Button>
    </form>
  );
};

LoginForm.propTypes = {
  className: PropTypes.string
};

export default LoginForm;
