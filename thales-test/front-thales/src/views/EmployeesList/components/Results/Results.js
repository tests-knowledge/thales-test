import React, { useEffect } from 'react';
import clsx from 'clsx';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tooltip,
  Typography,
  LinearProgress,
} from '@material-ui/core';
import getInitials from 'utils/getInitials';


const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1),
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  },
  icon: {
    fontSize: 18,
    height: 18,
    width: 18,
    paddingRight: '3px'
  },
}));

const Results = props => {
  const { className, employees, page, rowsPerPage, totalReg, onClickChangePage, onClickRowsPerPage, ...rest } = props;

  const classes = useStyles();

  useEffect(() => {
  }, []);

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      
      <Typography
        color="textSecondary"
        gutterBottom
        variant="body2"
      >
        {totalReg} Registros encontrados. Página {page} de {Math.round(totalReg/rowsPerPage)}
      </Typography>
      <Card>
        <CardHeader
          title="Todos los Empleados"
        />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center">Id</TableCell>
                    <TableCell align="center">Avatar</TableCell>
                    <TableCell align="center">Nombre</TableCell>
                    <TableCell align="center">Edad</TableCell>
                    <TableCell align="center">Salario</TableCell>
                    <TableCell align="center">Salario Anual</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {employees?.map(employee => (
                    <TableRow
                      hover
                      key={employee.idEmployee}
                    >
                      <TableCell align="center">
                        {employee.idEmployee}
                      </TableCell>
                      <TableCell align="center">
                        <div className={classes.nameCell}>
                          <Tooltip
                            placement="center"
                            title={employee.employeeName}
                          >
                            <Avatar
                              className={classes.avatar}
                              maxInitials={2}
                              name={employee.employeeName}
                              // src={employee.employeeName}
                            >
                              {getInitials(employee.employeeName)}
                              {/* {employee.employeeName} */}
                            </Avatar>
                          </Tooltip>
                        </div>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6">
                          {employee.employeeName}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6">
                          {employee.employeeAge}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6">
                          {employee.employeeSalary}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography variant="h6">
                          {employee.employeeAnualSalary}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              {props.loading ? (
                <LinearProgress />
              ): ''}
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={totalReg}
            labelDisplayedRows={
              () => {
                // return '' + from + '-' + to + ' de ' + count
                return `${((rowsPerPage)*(page-1)+1)} - ${totalReg < ((rowsPerPage)*(page-1)+rowsPerPage) ? totalReg : ((rowsPerPage)*(page-1)+rowsPerPage)} de ${totalReg}`
              }
            }
            labelRowsPerPage={'Registros por página:'}
            onChangePage={onClickChangePage}
            onChangeRowsPerPage={onClickRowsPerPage}
            page={(page-1)}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[5, 10, 25, 50]}
          />
        </CardActions>
      </Card>
      {/* <TableAssignBar selected={selectedTasks} /> */}
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  employees: PropTypes.array.isRequired,
  onChangePage: PropTypes.func,
  onChangeRowsPerPage: PropTypes.func,
};

Results.defaultProps = {
  employees: []
};

const mapStateToProps = (state) => {
  return {
    assignment: state.assignment,
    employeeStatus: state.employees?.employeeStatus
  };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Results);
