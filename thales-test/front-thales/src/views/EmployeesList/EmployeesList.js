import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import axios from 'utils/axios';
import { Page, SearchBar } from 'components';
import { Header, Results } from './components';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const ThirdPartiesManagementList = () => {
  const classes = useStyles();

  const [employees, setEmployees] = useState([]);
  const [loading, setLoading] = useState(1);
  const [petition, setPetition] = useState(true);
  const [petitionSearch, setPetitionSearch] = useState(true);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [totalReg, setTotalReg] = useState(0);
  const [page, setPage] = useState(1);

  useEffect(() => {
    let mounted = true;

    if(mounted){
      fetchEmployees(page, rowsPerPage);
    }

    return () => {
      mounted = false;
    };
  }, [petition]);

  useEffect(() => {
    let mounted = true;

    if(mounted){
      fetchOneEmployee(page, rowsPerPage);
    }

    return () => {
      mounted = false;
    };
  }, [petitionSearch]);

  const fetchEmployees = (page, rowsPerPage) => {
    axios.get('/thales/employee/', getAuthorization()).then((response) => {
      return response.data
    }).then((res) => {
      if(res.status == 200){
        setLoading(0);
        setPetition(false);
        setTotalReg(res.data?.length);
        setPage(page);
        setRowsPerPage(rowsPerPage);
        setEmployees(res.data?.slice(((rowsPerPage)*(page-1)), ((rowsPerPage)*(page-1)+rowsPerPage)));
      } else{
        setLoading(1);
        setPetition(petition !== true);
      }
    })
    
  };

  const fetchOneEmployee = (page, rowsPerPage, id) => {
    axios.get(`/thales/employee/${id}`, getAuthorization()).then((response) => {
      return response.data
    }).then((res) => {
      if(res.status == 200){
        var data = [];
        if(res.data != null) {
          data.push(res.data);
        }
        setLoading(0);
        setPetitionSearch(false);
        setTotalReg(data?.length);
        setPage(page);
        setRowsPerPage(rowsPerPage);
        setEmployees(data);
      } else{
        setLoading(1);
        setPetitionSearch(petitionSearch !== true);
      }
    })
    
  };

  const handleFilter = () => {

  };
  const handleSearch = (event) => {
    setLoading(1);
    if(event == ''){
      fetchEmployees(page, rowsPerPage);
    } else{
      if(event != undefined){
        fetchOneEmployee(page, rowsPerPage, event);
      }
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage+1);
    console.log('newPage: ', newPage+1);
    fetchEmployees((newPage+1), rowsPerPage);
  };

  const handleChangeLimit = (event) => {
    setRowsPerPage(event.target.value);
    console.log('limit: ', event.target.value);
    fetchEmployees(page, event.target.value);
  };

  return (
    <Page
      className={classes.root}
      title="Employees"
    >
      <Header />
      <SearchBar
        onFilter={handleFilter}
        onSearch={handleSearch}
      />
      {employees && (
        <Results
          className={classes.results}
          employees={employees}
          loading={loading}
          onClickChangePage={handleChangePage}
          onClickRowsPerPage={handleChangeLimit}
          page={page}
          petition={petition}
          rowsPerPage={rowsPerPage}
          setLoading={setLoading}
          setTotalReg={setTotalReg}
          totalReg={totalReg}
        />
      )}
    </Page>
  );
};

export default ThirdPartiesManagementList;
