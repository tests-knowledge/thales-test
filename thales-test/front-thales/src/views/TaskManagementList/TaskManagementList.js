/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';

import axios from 'utils/axios';
import { Page } from 'components';
import { Header, Results, SearchBar } from './components';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const ThirdPartiesManagementList = () => {
  const classes = useStyles();

  const [tasks, setEmployees] = useState([]);
  const [pending, setPending] = useState(null);
  const [loading, setLoading] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [totalReg, setTotalReg] = useState(0);
  const [page, setPage] = useState(0);
  const [keyword, setKeyword] = useState('');
  const [filters, setFilters] = useState([]);
  const [petition, setPetition] = useState(true);

  const dataStates = [
    { 'taskPriority': 0 },
    { 'taskPriority': 1 },
    { 'taskPriority': 2 },
    { 'taskPriority': 3 },
    { 'taskPriority': 4 },
    { 'taskPriority': 5 },
    { 'taskPriority': 6 },
    { 'taskPriority': 7 },
    { 'taskPriority': 8 },
    { 'taskPriority': 9 },
    { 'taskType': 'Primary'},
    { 'taskType': 'Secondary'},
    { 'taskType': 'Calendar'},
  ];

  const fetchEmployees = async (body = { page: page+1, limit: rowsPerPage }) => {
    setLoading(1);
    try {
      const petition = await axios.post('/tasks/pagination', body, getAuthorization());
      if (petition.data) {
        setEmployees(petition.data);
        setLoading(0);
        setTotalReg(petition.data.length);
        setPage(petition.data.page-1);
        setRowsPerPage(petition.data.per_page);
      } 
      setLoading(0);
    } catch (error) {
      setLoading(0);
    }
  };

  const bodyFunction = (limit, page, searchLocal = '',filtersLocal = []) => {
    const data = [];
    const columnsNeeded = {
      'activityId': 1,
      'projectId': 1,
      'type': 1,
      'area': 1,
      'status': 1,
      'thalesStatus': 1,
      'author': 1,
      'deadlineDate': 1,
      'createdAt': 1,
      'orderType': 1
    }
    let body = {
      'limit': limit,
      'page': page,
      'neq': [{'field': 'hide', 'values': [true]}, {'field': 'thalesStatus', 'values': ['Terminated']}],
      'sort': { column: 'deadlineDate', value: 1 }
    };
    if(searchLocal !== '') {
      for (var i in columnsNeeded) data.push({ 'field': [i], 'value': searchLocal});
      body = { ...body, 'or': data };
    }
    if(filtersLocal.length !== 0) {
      body = { ...body, 'and': filtersLocal};
    }
    return body;
  }

  const handleChangePage = async (event, newPage) => {
    const body = bodyFunction(rowsPerPage, (newPage + 1), keyword, filters);
    fetchEmployees(body);
    // fetchEmployees({'page': newPage+1, 'limit': rowsPerPage});
  };

  const handleChangeLimit = event => {
    const body = bodyFunction(event.target.value, (page + 1), keyword, filters);
    fetchEmployees(body);
  };

  useEffect(() => {
    let mounted = true;
    
    const fetchEmployees = async () => {
      axios.get('/thales/employee/', getAuthorization()).then((response) => {
        return response.data
      }).then((res) => {
        if(res.status == 200){
          setLoading(0);
          setPetition(false);
        } else{
          setLoading(1);
          setPetition(true);
        }
        console.log('res Task: ', res);
      })
    };

    if(mounted){
      fetchEmployees();
    }
    

    return () => {
      mounted = false;
    };
  }, [petition]);

  const handleFilter = (e) => {
    const filtersLocal = [];
    if (e.createAt[0] !== '' || e.createAt[1] !== '') filtersLocal.push({ 'field': 'createAt', 'value': e.createAt[0] !== '' && e.createAt[1] !== '' ? e.createAt : (e.createAt[0] === '' ? [e.createAt[1]] : [e.createAt[0]]) });
    if (e.updateAt[0] !== '' || e.updateAt[1] !== '') filtersLocal.push({ 'field': 'updateAt', 'value': e.updateAt[0] !== '' && e.updateAt[1] !== '' ? e.updateAt : (e.updateAt[0] === '' ? [e.updateAt[1]] : [e.updateAt[0]]) });
    if (e.author !== '' && e.author !== undefined) filtersLocal.push({ 'field': 'author', 'value': e.author });
    if (e.projectId !== '' && e.projectId !== undefined) filtersLocal.push({ 'field': 'projectId', 'value': e.projectId });
    if (e.activityId !== '' && e.activityId !== undefined) filtersLocal.push({ 'field': 'activityId', 'value': e.activityId });
    if (e.type.length !== 0) filtersLocal.push({ 'field': 'type', 'value': e.type });
    if (e.priority.length !== 0) filtersLocal.push({ 'field': 'priority', 'value': e.priority });
    if (e.area.length !== 0) filtersLocal.push({ 'field': 'area', 'value': e.area });
    setFilters(filtersLocal);
    const body = bodyFunction(rowsPerPage, 1, keyword, filtersLocal);
    fetchEmployees(body);
  };
  
  const handleSearch = (e) => {
    setKeyword(e);
    //Aqui va filters como 4 parametro
    const body = bodyFunction(rowsPerPage, 1, e);
    fetchEmployees(body);
  };

  if (pending) {
    return <Redirect to={'/tasks/backlog'} />
  }

  return (
    <Page
      className={classes.root}
      title="Tareas"
    >
      <Header />
      <SearchBar
        onFilter={handleFilter}
        onSearch={handleSearch}
        states={dataStates}
        value={keyword}
      />
      {tasks && (
        <Results
          className={classes.results}
          data={tasks}
          loading={loading}
          onClickChangePage={handleChangePage}
          onClickRowsPerPage={handleChangeLimit}
          page={page}
          rowsPerPage={rowsPerPage}
          setLoading={setLoading}
          setTotalReg={setTotalReg}
          totalReg={totalReg}
        />
      )}
    </Page>
  );
};

export default ThirdPartiesManagementList;
