// Se debe agregar una validacion para el estado previo, cuando se le de guardar y el estado previo es igual al estado seleccionado se debe agregar una nueva fecha como nombre alertDate

import React, { Fragment, useState, useEffect } from 'react';
import clsx from 'clsx';
import moment from 'moment';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import getAuthorization from 'utils/getAuthorization';
import { useSnackbar } from 'notistack';
import CloseIcon from '@material-ui/icons/Close';
import {
  Button,
  CircularProgress,
  Dialog,
  TextField,
  Typography,
  Grid,
  IconButton,
  colors
} from '@material-ui/core';
import axios from 'utils/axios';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import formatDate from 'utils/formatDate';
import { DateTimePicker } from '@material-ui/pickers';
import useRouter from 'utils/useRouter';


const useStyles = makeStyles(theme => ({
  root: {
    width: 960
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '5% auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  list: {
    maxHeight: 200
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  loader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  textField: {
    marginTop: theme.spacing(2),
  },
  title: {
    marginTop: theme.spacing(2),
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  }
}));

const Application = props => {
  const { logs, currentStatus, type, open, onClose, onApply, className, task, ...rest } = props;
  

  const classes = useStyles();
  const { history } = useRouter();
  console.log('task: ', task);
  console.log('type: ', type);
  const [value, setValue] = useState({name: currentStatus});
  const [info, setInfo] = useState({ status: currentStatus, description: '', deadlineDate: task.deadlineDate });
  const [data, setData] = useState([]);
  const [saving, setSaving] = useState(false);
  const [status, setStatus] = useState(null);
  const [expanded, setExpanded] = useState(false);
  const [calendarTrigger, setCalendarTrigger] = useState(null);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();  

  const handleChange = event => {
    event.persist();

    setInfo(info => ({
      ...info,
      [event.target.name]: event.target.value
    }));
  };

  const handleDismiss = () => {
    closeSnackbar();
  };

  const handleSubmit = async (event) => {
    if (document.forms[0].checkValidity()) {
      event.preventDefault();
      setSaving(true)
      const areaData = status.status.find((status) => status.value === info.status)

      const dataLog = logs
      const date = moment().format('YYYY-MM-DD HH:mm')
      await dataLog.push({ status: info.status, author: 'Leonel Oliveros#1032470120', description: info.description, createdAt: date})
      const dataForm = {
        thalesStatus: info.status,
        message: info.description,
        author: 'Leonel Oliveros#1032470120',
        logs: dataLog,
        updatedAt: date,
        hide: areaData?.end,
        finishDate: areaData?.end && date
      }
      const action = (
        <IconButton
          className={classes.expand}
          onClick={handleDismiss}
        >
          <CloseIcon />
        </IconButton>
      );
      axios.put(`/tasks/${task._id}`, { field: '_id', ...dataForm}, getAuthorization()).then(async (res1) => { 
        enqueueSnackbar('Tarea actualizada exitosamente', {
          variant: 'success',
          action
        });
        setSaving(false)
        onApply()
        history.go(0)
      })
    }
  }

  const handleChangeMore = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleCalendarOpen = trigger => {
    setCalendarTrigger(trigger);
  };

  const handleCalendarChange = (date) => {
    // date= moment(date,'DD/MM/YYYY');
    setInfo(info => ({
      ...info,
      [calendarTrigger]: moment(date).format('YYYY-MM-DD HH:mm')
    }));
  };

  const handleCalendarAccept = date => {
    
  };

  const handleCalendarClose = () => {
    setCalendarTrigger(false);
  };
  useEffect(() => {
    let mounted = true;
    
    const fetchStatus = () => {
      axios.get(`/areas?limit=1&page=1&name=${task.area}&group=${task.group}`, getAuthorization()).then(response => {
        if (mounted) {
          setData(response.data.data[0])
          let values = []
          if (task.role === 'Supervisor') {
            values = response.data.data[0].kpiStatus.filter((task) => task.type === type)
          } else {
            values = response.data.data[0].tasks.filter((task) => task.type === type)
          }
          const order = values[0]?.status.find((status) => status.value === currentStatus)
          setValue({ name: currentStatus, order: order?.order })
          setStatus(values[0]);
        }
      });
    };
    
    fetchStatus();
    

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const calendarOpen = Boolean(calendarTrigger);
  const calendarMinDate = moment();
  const calendarValue = info[calendarTrigger];

  const deadline = (date) => {
    const dline = moment(date, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD')
    const today = moment().format('YYYY-MM-DD')

    if (dline === today) {
      const newDate = moment(date, 'YYYY-MM-DD HH:mm').add(1, 'days')
      return `${formatDate(newDate).date} ${formatDate(newDate).time}`
    } else {
      return `${formatDate(date).date} ${formatDate(date).time}`
    }
    
  }

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <Grid
        alignItems="center"
        container
        spacing={2}
      >
          
        
        <form
          {...rest}
          className={clsx(classes.root, className, 'form')}
        >
          <div className={classes.header}>
            <Typography
              align="center"
              className={classes.title}
              gutterBottom
              variant="h3"
            >
            Cambie el Estado {type.split('#')[1]}
            </Typography>
            <Typography
              align="center"
              className={classes.subtitle}
              variant="subtitle2"
            >
            Selecciona un estado de la lista.
            </Typography>
          </div>
          <div className={classes.content}>
            { (status?.status && !saving) ? (
              <div>
                <TextField
                  SelectProps={{ native: true }}
                  fullWidth
                  label={type}
                  name="status"
                  onChange={event =>
                    handleChange(
                      event,
                      'status',
                      event.target.value
                    )
                  }
                  required
                  // eslint-disable-next-line react/jsx-sort-props
                  select
                  value={value.status}
                  variant="outlined"
                >
                  <option
                    disabled
                    value=""
                  />
                  {status.status.map(option => (
                    <option
                      disabled={(value.order > option.order)}
                      value={option.value}
                    >
                      {`${option.name}`}
                    </option>
                  ))}
                </TextField>
                <TextField
                  FormHelperTextProps={{ classes: { root: classes.helperText } }}
                  autoFocus
                  className={classes.textField}
                  // eslint-disable-next-line react/jsx-sort-props
                  fullWidth
                  helperText={`${200 - info.description.length} caracteres restantes`}
                  label="Nota corta"
                  multiline
                  name="description"
                  onChange={handleChange}
                  placeholder="Ingrese una razón"
                  required
                  rows={5}
                  variant="outlined"
                />
                <TextField
                  className={classes.dateField}
                  fullWidth
                  label={'Próxima Notificación'}
                  name="deadlineDate"
                  onClick={() => handleCalendarOpen('deadlineDate')}
                  required
                  value={deadline(info.deadlineDate)}
                  variant="outlined"
                />
                {logs.length > 0 && (
                  <Fragment className={classes.textField}>
                    <Typography
                      align="start"
                      className={classes.title}
                      gutterBottom
                      variant="h6"
                    >
                Estados Anteriores
                    </Typography>
                    <PerfectScrollbar
                      className={classes.list}
                      options={{ suppressScrollX: true }}
                    >
                      {logs.map((file, i) => (
                        <Accordion
                          expanded={expanded === `panel${i}`}
                          onChange={handleChangeMore(`panel${i}`)}
                        >
                          <AccordionSummary
                            aria-controls="panel1bh-content"
                            expandIcon={<ExpandMoreIcon />}
                            id="panel1bh-header"
                          >
                            <Typography className={classes.heading}>{`${formatDate(file.createdAt).date} ${formatDate(file.createdAt).time}`}</Typography>
                            <Typography className={classes.secondaryHeading}>{`${file.author.split('#')[0]} / ${file.status}`}</Typography>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Typography>
                              {file.description}
                            </Typography>
                          </AccordionDetails>
                        </Accordion>
                      ))}
                    </PerfectScrollbar>
                  </Fragment>
                )}
                <DateTimePicker
                  minDate={calendarMinDate}
                  onAccept={handleCalendarAccept}
                  onChange={handleCalendarChange}
                  onClose={handleCalendarClose}
                  open={calendarOpen}
                  style={{ display: 'none' }} // Temporal fix to hide the input element
                  value={calendarValue}
                  variant="dialog"
                />
              </div> 
            ) : (
              <div className={classes.loader}>
                <CircularProgress color="secondary" />
                { saving ? (
                  <Typography>Guardando</Typography>
                ) : (
                  <Typography>Buscando estados...</Typography>
                )}
                
              </div>
            )
            }
          </div>
          <div className={classes.actions}>
            <Button
              onClick={onClose}
              variant="contained"
            >
            Cerrar
            </Button>
            { !saving && (
              <Button
                className={classes.applyButton}
                onClick={handleSubmit}
                type="submit"
                variant="contained"
              >
              Guardar
              </Button>
            )}
          </div>
        </form>
      </Grid>
    </Dialog>
  );
};

Application.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

export default Application;
