import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  IconButton,
  Divider,
  Button,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tooltip,
  Typography,
  LinearProgress,
  colors
} from '@material-ui/core';
import moment from 'moment';
import ScheduleIcon from '@material-ui/icons/Schedule';
import { Label, ReviewStars } from 'components';
import { Application, StatusChange, Logs } from './components';
import getTimeStatus from 'utils/getTimeStatus';
import formatDate from 'utils/formatDate';
import duration from 'utils/duration';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import { useTranslation } from 'react-i18next';
import { tasksStatus } from 'actions';
import ListAltIcon from '@material-ui/icons/ListAlt';
import Cookies from 'js-cookie';
import { useSnackbar } from 'notistack';


const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  },
  icon: {
    fontSize: 18,
    height: 18,
    width: 18,
    paddingRight: '3px'
  },
}));

const Results = props => {
  const { className, data, assignment, taskStatus, page, rowsPerPage, totalReg, onClickChangePage, onClickRowsPerPage, setTotalReg, ...rest } = props;

  const classes = useStyles();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [tasks, setTasks] = useState(data);
  // const [tasks, setRowsPerPage] = useState(data.per_page);
  const [openApplication, setOpenApplication] = useState(false);
  const [openStatusChange, setOpenStatusChange] = useState(false);
  const [openLogs, setOpenLogs] = useState(false);
  const [oneTask, setOneTask] = useState({});

  const showButton = (type, task) => {
    switch (type) {
      case 'PC':
        return (
          <Button
            color="primary"
            component={RouterLink}
            size="small"
            to={`/format/${task._id}/${task.projectId}/${task.activityId}`}

            variant="outlined"
          >
            Cerrar
          </Button>
        )
      case 'AS':
        return (
          <Button
            color="primary"
            size="small"
            onClick={() => handleApplicationOpen(task)}
            variant="outlined"
          >
            Asignar
          </Button>
        )
        case 'null':
          return (
            <Button
              color="primary"
              component={RouterLink}
              size="small"
              to="/calendar"
              variant="outlined"
            >
              Actualizar
            </Button>
          )
      default: 
        return (
          <Button
            color="primary"
            size="small"
            onClick={() => handleStatusChangeOpen(task)}
            variant="outlined"
          >
            Actualizar
          </Button>
        )
    }
  }

  const handleAsignClick = async (usuario, task) => {
    const nombre = Cookies.get('firstName');
    const apellido = Cookies.get('lastName');
    const identificacion = Cookies.get('identification');
    const idObject = task._id;
    const newlog = {
      status: 'Asignación#AS',
      createdAt: moment().format('YYYY-MM-DD HH:mm'),
      description: `Tarea asignada a Ejecutor ${usuario.split('#')[0]} othId: ${task.activityId}`,
      author: `${nombre} ${apellido}#${identificacion}`,
    }
    const logAux = task.logs ? task.logs : [];
    logAux.push(newlog);
    if (usuario === '') {
      enqueueSnackbar(`Debe seleccionar un Usuario.`, {
        variant: 'error',
      });
    } else {
      delete task._id;
      await axios.put(`/tasks/${idObject}`,
                {
                  field: '_id',
                  updateNotify: `${nombre} ${apellido}#${identificacion}`,
                  author: usuario,
                  message: `Tarea asignada a ${usuario.split('#')[0]}`,
                  logs: logAux,
                  role: 'Ejecutor',
                },
                getAuthorization())
                .then(() => {
                    enqueueSnackbar(`Tarea asignada a ${usuario.split('#')[0]}`, {
                      variant: 'success',
                    });
                    window.location.href = '/management/tasks';
                })
                .catch((err) => {
                  enqueueSnackbar(`error: ${err}`, {
                    variant: 'error',
                  });
                })
                setOpenApplication(false);
    }
  };

  const handleApplicationOpen = (tsk) => {
    setOneTask(tsk)
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const handleStatusChangeOpen = (task) => {
    dispatch(tasksStatus({status: task.status, current: task.thalesStatus, logs: task.logs, task}));
    setOpenStatusChange(true);
  };

  const handleStatusChangeClose = () => {
    setOpenStatusChange(false);
  };

  const handleLogsOpen = (task) => {
    dispatch(tasksStatus({status: task.status, current: task.thalesStatus, logs: task.logs, task}));
    setOpenLogs(true);
  };

  const handleLogsClose = () => {
    setOpenLogs(false);
  };

  const taskStatusColors = {
    canceled: colors.grey[600],
    warning: colors.orange[600],
    onTime: colors.green[600],
    outOfTime: colors.red[600]
  };

  const statusColors = {
    Assigned: colors.orange[600],
    Canceled: colors.red[600],
    Terminated: colors.green[600],
    Created: colors.grey[600]
  };

  useEffect(() => {
    setTasks(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      
      <Typography
        color="textSecondary"
        gutterBottom
        variant="body2"
      >
        {tasks.total} Registros encontrados. Página {page + 1} de {tasks.total_pages}
      </Typography>
      <Card>
        <CardHeader
          title="Todas las Actividades"
        />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>                    
                    <TableCell></TableCell>
                    <TableCell align="center">Información</TableCell>
                    <TableCell align="center">Fecha de Entrega</TableCell>
                    <TableCell align="center">Fecha de Creación</TableCell>
                    <TableCell align="center">Responsable</TableCell>
                    <TableCell align="center">Estado</TableCell>
                    <TableCell align="center">Terminado</TableCell>
                    <TableCell align="center">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {tasks.data?.map(task => (
                    <TableRow
                      hover
                      key={task.id}
                    >
                      <TableCell align="center">
                        <ReviewStars value={task.priority} calendar={task.type} />
                      </TableCell>
                      <TableCell align="left">
                        <div className={classes.nameCell}>
                          <Tooltip title={task.status?.split("#")[0]} placement="left">
                            <Avatar
                              className={classes.avatar}
                              src={task.avatar}
                            >
                              {task.status?.split("#")[1]}
                            </Avatar>
                          </Tooltip>
                          <div>
                            <Typography variant="body2">
                               {task.activityId && `OTH: ${task.activityId}`}
                               {task.projectId && ` OTP: ${task.projectId}`}
                            </Typography>
                            <Tooltip title={'Registros'} placement="left">
                              <IconButton onClick={() => handleLogsOpen(task)} color={'primary'}>
                                <ListAltIcon />
                              </IconButton>
                            </Tooltip>
                            <Link
                              color="inherit"
                              component={RouterLink}
                              onClick={handleApplicationOpen}
                              variant="h6"
                            >
                              {task.orderType}
                            </Link>
                            <Typography variant="body2">
                              <Label
                                color={taskStatusColors[getTimeStatus(task.deadlineDate, task.finishDate && task.finishDate)]}
                                variant="outlined"
                              >
                                <ScheduleIcon className={classes.icon}/>{t(getTimeStatus(task.deadlineDate, task.finishDate && task.finishDate))}
                              </Label>
                            </Typography>
                          </div>
                        </div>
                      </TableCell>
                      <TableCell align="center">
                        {formatDate(task.deadlineDate).date}
                        <Typography variant="h6">
                        {formatDate(task.deadlineDate).time}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        {formatDate(task.createdAt).date}
                        <Typography variant="h6">
                        {formatDate(task.createdAt).time}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography
                          variant="h6"
                        >
                          {task.author?.split('#')[0]}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography
                          style={{ color: statusColors[task.thalesStatus] }}
                          variant="h6"
                        >
                          {t(task.thalesStatus)}
                          <Typography>
                          {task.finishDate && `${formatDate(task.finishDate).date} ${formatDate(task.finishDate).time}`}
                          </Typography>
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        {duration(task.createdAt, task.finishDate)}
                      </TableCell>
                      <TableCell align="center">
                      {task.thalesStatus !== 'Terminated' && (
                        showButton(task.status.split("#")[1], task)
                      )}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              {props.loading ? (
                <LinearProgress />
              ): ''}
              { openApplication && (
                <Application 
                onApply={handleAsignClick}
                onClose={handleApplicationClose}
                open={openApplication}
                task={oneTask}
              />
              )}
              { (openStatusChange && taskStatus) && (
                <StatusChange 
                  onApply={handleStatusChangeClose}
                  onClose={handleStatusChangeClose}
                  open={openStatusChange}
                  currentStatus={taskStatus.current}
                  type={taskStatus.status}
                  logs={taskStatus.logs}
                  task={taskStatus.task}
                />
              )}
              { (openLogs) && (
                <Logs 
                  onApply={handleLogsClose}
                  onClose={handleLogsClose}
                  open={openLogs}
                  currentStatus={taskStatus.current}
                  type={taskStatus.status}
                  logs={taskStatus.logs}
                  task={taskStatus.task}
                />
              )}
              
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={tasks.total}
            onChangePage={onClickChangePage}
            onChangeRowsPerPage={onClickRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[10, 25, 50]}
            labelDisplayedRows={
              ({ from, to, count }) => {
                return '' + from + '-' + to + ' de ' + count
              }
            }
            labelRowsPerPage={'Registros por página:'}
          />
        </CardActions>
      </Card>
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  tasks: PropTypes.array.isRequired
};

Results.defaultProps = {
  tasks: []
};

const mapStateToProps = (state) => {
  return {
    assignment: state.assignment,
    taskStatus: state.tasks.taskStatus
  };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Results);
