import React, { Component } from 'react';
import EditorJs from 'react-editor-js';
import moment from 'moment';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';
import { EDITOR_JS_TOOLS } from 'utils/constants';
import { saveCkList, saveCkFormat } from 'actions';
import { getToken } from 'tokenUtils';

class TextEditor extends Component {
  token = getToken();

  userName = `${Cookies.get('firstName')} ${Cookies.get('lastName')}`;

  userId = Cookies.get('identification');

  save = () => {
    // eslint-disable-next-line react/destructuring-assignment
    this.props.saveCkList(undefined);
  };

  saveFormat = (data) => {
    // eslint-disable-next-line react/destructuring-assignment
    this.props.saveCkFormat(
      this.token,
      // eslint-disable-next-line react/destructuring-assignment
      this.props.CkList.othId,
      {
        createdBy: this.userName,
        updatedAt: moment().format('YYYY-MM-DD HH:mm'),
        createdAt: moment().format('YYYY-MM-DD HH:mm'),
        format: data,
      },
    );
  };

  async handleSave() {
    const savedData = await this.editorInstance.save();
    this.saveFormat(savedData);
  }

  activeEditor = (service) => {
    switch (service) {
      case 'internet_NAP_Diferenciado':
      case 'internet_Dedicado':
      case 'internet_Dedicado_Administrado':
      case 'internet_Empresarial':
      case 'internet_Pymes_Banda_Ancha': /* CONFIGURACION  INTERNET v2.3 */
        return {
          'time': 1580397997606,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t\tINTERNET DEDICADO ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD L2 (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad del puerto en equipo de acceso (Si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD VLAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad de vlan en PE y SW de acceso (si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Pruebas de configuración a nivel de L2 (se debe documentar el BW configurado)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En caso de ser GPON thales PON documentar el resultado del comando show onu run conf gpon-onu_1/X/X:X] ', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Se debe asegurar',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Marcacicón acorde al estandar de la plataforma',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar capacidad ajustada de BW (CAR)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar modos de TX ajustados en el pto',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  ASIGNACIÓN DIRECCIONAMIENTO ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RED WAN:\t\t./30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP PUBLICA:\t\t./2x',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN WAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN público asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN LAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento LAN  en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  // Ejecutar (y reemplazar en la documentación) el siguiente procedimiento de verificación de enlistamiento de direccionamiento público',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '--------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'C&gt;nslookup x.x.x.x (ingresar IPs. a validar, se espera que el resultado sea estatic o dinamic)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Ingresar a: https://mxtoolbox.com/blacklists.aspx (Validar las IPs, Si aparece con X quiere decir que esta en lista negra)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Para mayor informacion ingresar al link: ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'https://embed.vidyard.com/share/GXWqFVMKnqj4PxC2yckste? ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '--------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFAZ/ENRUTAMIENTO PE (L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia de configuración de subinterface en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia deconfiguración de enrutamiento en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si el enrutamiento es bgp en ASR, se debe documentar el route-policy y el prefix-set]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe  utilizar el activador corporativo (HPSA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  De no ejecutarse por activador, documentar el correo de reporte del no uso del HPSA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION ADICIONAL (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Información a terceros, configuraciones atípicas o pendientes',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar pendientes. ej. creación de túneles SDP]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si se requiere módulo, documentar correo]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR completa y actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar plataforma intermedia a nivel de L2 Alcatel (BW, VLAN, Service ID)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  BITACORA AVAL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar Bitacora de la configuración realizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectación del servicio\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '---------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel técnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la información de acuerdo a lo solicitado en el check de configuración y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'datos_MPLS_Intranet':
      case 'datos_MPLS_Extranet':
      case 'datos_Backup_HSRP':
      case 'datos_Backup_UM_sobre_un_mismo_CPE': /* CONFIGURACIрN MPLS v2.3 */
        return {
          'time': 1580398958133,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': 'CONFIGURACION  MPLS INTRANET/EXTRANET  ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VALIDACION LAN (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de segmento LAN por enrutamiento en el PC]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Seguir procedimiento definido para el caso en el archivo disponibilidad cargado en el SP',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD L2 (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad del puerto en equipo de acceso (Si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar prueba de modulo instalado en el puerto seleccionado (si Aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD VLAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad de vlan en PE y SW de acceso (si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Pruebas de configuración a nivel de L2 (se debe documentar el BW configurado)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En caso de ser GPON thales PON documentar el resultado del comando show onu run conf gpon-onu_1/X/X:X] ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Se debe asegurar',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Marcacicón acorde al estandar de la plataforma',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar capacidad ajustada de BW (CAR)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar modos de TX ajustados en el pto',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  ASIGNACIÓN DIRECCIONAMIENTO ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en A9KTRIARA1 por la vrf cpemanagement]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- RED WAN:\t\t/30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN WAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN público asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFACE/ENRUTAMIENTO PE (L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia de configuración de subinterface en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia deconfiguración de enrutamiento en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si el enrutamiento es bgp en ASR, se debe documentar el route-policy y el prefix-set]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe  utilizar el activador corporativo (HPSA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  De no ejecutarse por activador, documentar el correo de reporte del no uso del HPSA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIONES ADICONALES (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Información a terceros, configuraciones atípicas o pendientes',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar pendientes. ej. creación de túneles SDP.]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si se requiere módulo, documentar correo]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR completa y actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar plataforma intermedia a nivel de L2 Alcatel (BW, VLAN, Service ID)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  BITACORA AVAL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar Bitácora de la configuración realizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel técnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la información de acuerdo a lo solicitado en el check de configuración y adjuntar soportes requeridos. (No debe aparecer en CRM ésta nota)****',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'videoconferencia_HD':
      case 'videoconferencia_Video_Desktop':
      case 'videoconferencia_Estandar': /* CONFIGURACION VIDEO CONFERENCIA */
        return {
          'time': 1580399345256,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\tCONFIGURACION VIDEO CONFERENCIA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'ENTRADAS:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- NOMBRE DE LA CAMARA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- EXTENSION A ASOCIAR ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\tJABBER',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- NOMBRE DEL USUARIO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- EXTENSION A ASOCIAR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'CONFIGURACION SEGUN MANUAL: TELEPRESENCIA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD L2 (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD VLAN\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- vLAN: ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VALIDACION SEGMENTO LAN VIDEO CONFERENCIA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE DEBE VALIDAR DISPONIBILIDAD DEL IPAM2',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '10.173.X.X/29',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  ASIGNACIÓN DIRECCIONAMIENTO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- RED WAN:\t\t/30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '|',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'DISPONIBILIDAD\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFACE PE/ENRUTAMIENTO PE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CUCM - CALL MANAGER\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© REGION\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© LOCATION\t\t\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© DEVICE POOL\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© PARTICION\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© CALLING SEARCH SPACE\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© MTP (PARA JABER) RECURSOS DE SOFTWARE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© MEDIA RESOURCE GROUP (RECURSOS TRP)\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© MEDIA RESOURCE GROUP LIST (RECURSOS TRP)\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© ROUTE GROUP\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© ROUTE LIST\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© ROUTE PATTERN\t\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© PHONES (cAMRAS O JABBER)\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© USERS\t(LDAP)\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© SERVICE PROFILE (ESCALAR IS CLARO)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© TRANSLATION PATTERN PARA INGRESO A LA SALAS (SOLICITAR RANGO IS CLARO(MULTICONFERENCIA))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Cuando se va a instalar Jabber se debe tener en cuenta:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- Configuracion en el CUPS (servidor de presencia) de directorio para JABBER.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'ORIGEN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CHECK PBX ADMINISTRADA\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'DATOS CONFIGURACION\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Red Asignada VIDEO\t:\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP Gateway\t\t:\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Tipo de dispositivo     : (camara/jabber).',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel tecnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la informacion de acuerdo a lo solicitado en el check de configuracion y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'telefonia_Publica_Lineas_Análogas_GAOKE':
      case 'telefonia_Publica_Lineas_Análogas_IAD':
      case 'telefonia_Pública_E1':
      case 'telefonia_Publica_SIP':
      case 'telefonia_Publica_R2':
      case 'telefonia_Corporativa':
      case 'telefonia_SIP_centralizado': /* CONFIGURACION TELEFONIA(SIP,E1,BASICA) v2.3 */
        return {
          'time': 1580400817866,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t*CONFIGURACION  TELEFONIA PUBLICA *',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD L2 (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad del puerto en equipo de acceso (si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD VLAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad de vlan en PE y SW de acceso (si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Pruebas de configuración a nivel de L2 (se debe documentar el BW configurado)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En caso de ser GPON thales PON documentar el resultado del comando show onu run conf gpon-onu_1/X/X:X] ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Se debe asegurar',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Marcacicón acorde al estandar de la plataforma',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar capacidad ajustada de BW (CAR)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar modos de TX ajustados en el pto',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  ASIGNACIÓN DIRECCIONAMIENTO ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- RED WAN:\t\t/30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN WAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN público asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©   DISPONIBILIDAD SEGMENTO TELEFONIA  (LOOPBACK)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- RED LOOPBACK: /29 0 30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN LAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento LAN  en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFACE/ENRUTAMIENTO PE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia de configuración de subinterface en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia deconfiguración de enrutamiento en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si el enrutamiento es bgp en ASR, se debe documentar el route-policy y el prefix-set]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe  utilizar el activador corporativo (HPSA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  De no ejecutarse por activador, documentar el correo de reporte del no uso del HPSA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  NUMERACIÓN ASIGNADA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar numeración cargada en el TAB de telefonía]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' N° TRONCAL:\t(SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' CABECERA\t(SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  DID',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' Numeracion telefonia Basica',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' NUMERACION ASOCIADA:                   \t(SI APLICA) ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' CABECERA PBX:\t\t\t(SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VERIFICACION NUMERACION Y RECURSOS EN EL SOFT',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad de la numeracion en el SSW]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe adjuntar como objeto ole en la OTP  la verificacion de la disponibilidad de la numeracion.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION PLATAFORMA DE TELEFONIA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de configuración de la troncal y numeración en la plataforma de telefonia (documentar los resultados completos)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si es por IMS o HST, se debe asegurar la documentacion del comando LST SPCNACLD para confirmar que la numeracion quedo bien configurada.]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PERMISOS DE MARCACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En los casos que se requiera permisos adicionales fuera de la salida local, se debe tener el soporte ya sea Survey o correo de autorizacion del cliente o comercial] de igual forma nivel 1 debe estar enterado del tema.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION ADICIONAL (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR completa y actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar plataforma intermedia a nivel de L2 Alcatel (BW, VLAN, Service ID)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  BITACORA AVAL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar Bitácora de la configuración realizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar pendientes. ej. creación de túneles SDP.]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si se requiere módulo, documentar correo]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©      CHEK LIST TELEFONIA  -',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentacion del check list de telfonia con la informacion del servicio ]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectación del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel técnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la información de acuerdo a lo solicitado en el check de configuración y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'telefonia_PBX_Distribuida_E1':
      case 'telefonia_PBX_Distribuida_SIP':
      case 'telefonia_PBX_Distribuida_SIP_Centralizado': /* CONFIGURACION PBX DISTRIBUIDA v2.3 */
        return {
          'time': 1580401422634,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\tCONFIGURACION - PBX DISTRIBUIDA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD L2 (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad del puerto en equipo de acceso (Si Aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD VLAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad de vlan en PE y SW de acceso (si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- vLAN: ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Pruebas de configuración a nivel de L2 (se debe documentar el BW configurado)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En caso de ser GPON thales PON documentar el resultado del comando show onu run conf gpon-onu_1/X/X:X] ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Se debe asegurar',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Marcacicón acorde al estandar de la plataforma',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar capacidad ajustada de BW (CAR)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar modos de TX ajustados en el pto',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  ASIGNACIÓN DIRECCIONAMIENTO ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- RED WAN:\t\t/30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN WAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN público asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© DISPONIBILIDAD LOOPBACK ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- RED LOOPBACK: /29 0 /30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN LAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento LAN  en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionaminto LAN asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el pro',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'cedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFACE/ENRUTAMIENTO  PE (L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia de configuración de subinterface en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia deconfiguración de enrutamiento en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si el enrutamiento es bgp en ASR, se debe documentar el route-policy y el prefix-set]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe  utilizar el activador corporativo (HPSA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  De no ejecutarse por activador, documentar el correo de reporte del no uso del HPSA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  NUMERACIÓN ASIGNADA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar numeración cargada en el TAB de telefonía]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' N° TRONCAL:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' CABECERA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'CIUDAD\tINDICAT\tNUMERO\t  DIALPLN\tPREFIX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VERIFICACION// CONFIGURACION SOFTSW',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad de la numeracion en el SSW]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe adjuntar como objeto ole en la OTP  la verificacion de la disponibilidad de la numeracion.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION PLATAFORMA DE TELEFONIA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de configuración de la troncal y numeración en la plataforma de telefonia (documentar los resultados completos)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si es por IMS o HST, se debe asegurar la documentacion del comando LST SPCNACLD para confirmar que la numeracion quedo bien configurada.]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PERMISOS DE MARCACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Por producto este servicio se debe configurar con permisos de salida solo local.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En los casos que se requiera permisos adicionales fuera de la salida local, se debe tener el soporte ya sea Survey o correo de autorizacion del cliente o comercial] de igual forma nivel 1 debe estar enterado del tema.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR completa y actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar plataforma intermedia a nivel de L2 Alcatel (BW, VLAN, Service ID)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  BITACORA AVAL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar Bitácora de la configuración realizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar pendientes. ej. creación de túneles SDP]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si se requiere módulo, documentar correo]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-----------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©      CHEK LIST TELEFONIA  -',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-----------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentacion del check list de telfonia con la informacion del servicio ]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-----------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-----------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectación del servicio\t\t\tSI/NO\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel técnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la información de acuerdo a lo solicitado en el check de configuración y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'privateLineService_STM1':
      case 'privateLineService_E1_Clear_Channel':
      case 'privateLineService_E3_Clear_Channel':
      case 'privateLineService_N64_Clear_Channel':
      case 'privateLineService_PL_Ethernet': /* CONFIGURACION PL ETH */
        return {
          'time': 1580407473532,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t\tPL ETHERNET',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PUERTO DE ACCESO ORIGEN (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PUERTO DE ACCESO DESTINO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO ORIGEN Y DESTINO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION ADICIONAL (QoS)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'ORIGEN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel tecnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la informacion de acuerdo a lo solicitado en el check de configuracion y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'privateLineService_PL_Ethernet_Atom': /* CONFIGURACION PL ETH ATOM */
        return {
          'time': 1580429391207,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t\t*PL ETHERNET*',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ORIGEN (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- VLAN: .',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- TIPO DE ENCABSULAMIENTO:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN ATOM PE ORIGEN (SI APLICA) ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'interface GigabitEthernet1/0/2.XX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' description IPDP ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' encapsulation dot1Q XXX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' xconnect 10.10.66.176 1028 encapsulation mpls',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' service-policy input CAR-4M',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' service-policy output CAR-4M',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'end',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO DESTINO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- VLAN: .',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- TIPO DE ENCABSULAMIENTO:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN ATOM PE DESTINO  (SI APLICA) ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'interface GigabitEthernet1/0/2.XX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' description IPDP ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' encapsulation dot1Q XXX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' xconnect 10.10.66.176 1028 encapsulation mpls',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' service-policy input CAR-4M',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' service-policy output CAR-4M',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'end',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION ADICIONAL (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'VALIDACION QUE SUBA EL PROTOCOLO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'ORIGEN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#sh xconnect interface ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#SH XCOnnect int ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CONFIGURACIONES ADICIONALES.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel tecnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la informacion de acuerdo a lo solicitado en el check de configuracion y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'novedad_Cambio_de_Velocidad_BW': /* NOVEDAD CAMBIO BW */
        return {
          'time': 1580425742288,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\tMODIFICACION ANCHO DE BANDA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tNuevo Ancho de banda:\t.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN PUERTO ACCESO(L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN INTERFACE (L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tVERIFICAR RESTRICCION CPE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tACTUALIZACION BW (L2-L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tPENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tPDSR/PSR (ACTUALIZADA)  ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tOBSERVACIONES:',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'novedad_Traslado_Externo':
      case 'novedad_Cambio_de_servicio':
      case 'novedad_Traslado_Interno':
      case 'Cambio_Velocidad_Adición_o_cambio_Equipo':
      case 'novedad_Cambio_o_Adicion_de_Equipo': /* CONFIGURACION TRANSLADO */
        return {
          'time': 1580427221090,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t\t*TRANSLADO DE SERVICIO*',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD L2 (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD VLAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFAZ/ENRUTAMIENTO PE (L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIONES ADICIONALES.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  LIBERACION DE RECURSOS EN LA PEM.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Procedimiento adjunto en el Archivo:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR/PSR ACTUALIZADA.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST TELEFONIA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel tecnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la informacion de acuerdo a lo solicitado en el check de configuracion y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'novedad_Adicion_Retiro_de_Numeración': /* ADICION-RETIRO DID - CANALES */
        return {
          'time': 1580427409691,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\tMODIFICACION DE NUMERACION PUBLICA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tACTUALIZACION L2-L3',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tMODIFICACION SOLICITADA SSW',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t- ADICION DE DID',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '        - ADICION DE CANALES',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tMODIFICACION CPE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© PSR/PDSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© OBSERVACIONES\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE REALIZA ADICION DE DID .',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE REALIZA APLIACION A .  CANALES',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'novedad_Liberacion_de_Recursos': /* LIBERACION RECURSOS */
        return {
          'time': 1580481651210,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t   LIBERACION RECURSOS',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©    VERIFICACION DE LA CONFIGURACION (L2 Y L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©    LIBERACION DE RECURSOS  PE\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©    LIBERACION DE RECURSOS  SWITCH ACCESO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©    LIBERACION DE RECURSOS  TELEFONIA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tPDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tOBSERVACIONES',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe liberar direccionamiento en el E-management.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'telefonia_PBX_Administrada': /* CONFIGURACION PBX ADMINISTRADA v2.3 */
        return {
          'time': 1580480596137,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\tCONFIGURACION  PBX ADMINISTRADA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD L2 (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad del puerto en equipo de acceso (Si Aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD VLAN\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad de vlan en PE y SW de acceso (si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- vLAN: ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Pruebas de configuración a nivel de L2 (se debe documentar el BW configurado)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En caso de ser GPON thales PON documentar el resultado del comando show onu run conf gpon-onu_1/X/X:X] ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Se debe asegurar',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Marcacicón acorde al estandar de la plataforma',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar capacidad ajustada de BW (CAR)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Asegurar modos de TX ajustados en el pto',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VALIDACION SEGMENTO LAN PBX ADMINISTRADA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN LAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento LAN  en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  ASIGNACIÓN DIRECCIONAMIENTO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- RED WAN:\t\t/30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN WAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN público asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFACE PE/ENRUTAMIENTO PE (L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia de configuración de subinterface en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia deconfiguración de enrutamiento en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si el enrutamiento es bgp en ASR, se debe documentar el route-policy y el prefix-set]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe  utilizar el activador corporativo (HPSA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  De no ejecutarse por activador, documentar el correo de reporte del no uso del HPSA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CUCM - CALL MANAGER\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Si aplica documentar las opcion requerida(Adjuntar plantilla HCS))\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© REGION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© LOCATION\t\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© DEVICE POOL\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© PARTICION\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© MEDIA RESOURCE GROUP\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© GATEWAY\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© FXO\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© FXS\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© SIP TRUNK SECURITY PROFILE\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© TRUNKS',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© ROUTE LIST\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© ROUTE PATTERN\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© HUNT PILOT\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© TRANSLATION PATTERN\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© PHONES\t\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© FORCED AUTHORIZATION CODE\t\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© UNITY\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© SYTEM CALL HANDLERS\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© DIREC ROUTING RULES\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t© VOICEMAIL\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR completa y actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar plataforma intermedia a nivel de L2 Alcatel (BW, VLAN, VC)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  BITACORA AVAL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar Bitácora de la configuración realizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CHECK PBX ADMINISTRADA\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'DATOS CONFIGURACION\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Vlan de Voz\t100\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Red Asignada Voz\t10.172.101.32/28\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP Gateway\t10.172.101.33/28\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'No.  Telefono\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Version IOS Router\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Music on Hold  Pagina de IOS Default/cliente\tIOS/PBXadmin/SampleAudioSource.wav\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP CCM Subscriber (Primario)\t172.31.237.180\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP CCM Publisher (Backup)\t172.31.237.162\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP servidor  NTP\t172.31.237.129\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Asignación de contraseñas\tUser -Voicemail 121314\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectación del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel técnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la información de acuerdo a lo solicitado en el check de configuración y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'datos_RG_Comcel': /* CONFIGURACIрN RGCOMCEL - ITX v2.3 */
        return {
          'time': 1580504185672,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\tCONFIGURACION  RGCOMCEL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VALIDACION LAN (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de segmento LAN por enrutamiento en el PC]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Seguir procedimiento definido para el caso en el archivo disponibilidad cargado en el SP',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD VLAN Y TUNNEL RGCOMCEL3G1-2',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de disponibilidad de vlan en PE y SW de acceso (si aplica)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  ASIGNACIÓN DIRECCIONAMIENTO ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en A9KTRIARA1 por la vrf cpemanagement]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '- RED WAN:\t\t/30',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' // VERIFICACIÓN WAN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validación de direccionamiento WAN en e-management por palabra]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN público asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN  SERVICIO EQUIPOS RGCOMCEL3G1-2 O ITX (HFCORTEZAL-HFCBOSQUE) (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t@ CONFIGURACION EQUIPO PPAL RGCOMCEL3G1',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t@ CONFIGURACION EQUIPO BACKUP RGCOMCEL3G2',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFACE/ENRUTAMIENTO RGCOMCEL3G1',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia de configuración de subinterface en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia deconfiguración de enrutamiento en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si el enrutamiento es bgp en ASR, se debe documentar el route-policy y el prefix-set]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe  utilizar el activador corporativo (HPSA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  De no ejecutarse por activador, documentar el correo de reporte del no uso del HPSA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFACE/ENRUTAMIENTO RGCOMCEL3G2',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia de configuración de subinterface en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia deconfiguración de enrutamiento en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si el enrutamiento es bgp en ASR, se debe documentar el route-policy y el prefix-set]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe  utilizar el activador corporativo (HPSA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  De no ejecutarse por activador, documentar el correo de reporte del no uso del HPSA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VERIFICACION PREFIJOS ENSEÑADOS A LA MPLS PPAL Y BACKUP',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Evidencia de redes LAN enseñadas a la MPLS por su respectiva vrf PPL  y BACKUP]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIONES ADICONALES (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Información a terceros, configuraciones atípicas o pendientes',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar pendientes. ej. creación de túneles SDP.]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si se requiere módulo, documentar correo]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR completa y actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar plataforma intermedia a nivel de L2 Alcatel (BW, VLAN, Service ID)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  BITACORA AVAL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar Bitácora de la configuración realizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel técnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la información de acuerdo a lo solicitado en el check de configuración y adjuntar soportes requeridos. (No debe aparecer en CRM ésta nota)****',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'datos_Suspension': /* PLANTILLA NOVEDAD SUSPENSION */
        return {
          'time': 1580504355926,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': ' SUSPENSION POR CARTERA/CLIENTE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CODIGO SERVICIO A SUSPENDER:  ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe documentar el código de servicio al cual se le va a realizar la suspensión',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VERIFICACIÓN SERVICIO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se documenta la información de la subinterface y enrutamiento configurado en el PE.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe confirmar que sea el servicio a afectar.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DESHABILITACION SERVICIO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se documenta el procedimiento de suspensión y marcación de la interface según manual de marcaciones.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIRMACION DE LA SUSPENSION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se documenta la información de la interfaz y enrutamiento después de la suspensión.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  OBSERVACIONES.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'datos_Reconexion': /* PLANTILLA NOVEDAD RECONEXION */
        return {
          'time': 1580504676859,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': 'RECONEXION SERVICIO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CODIGO SERVICIO A RECONECTAR:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe documentar el código de servicio al cual se le va a realizar la suspensión',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tREVISION CONFIGURACION  SERVICIO ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se documenta la información actual de la subinterface y enrutamiento configurado en el PE.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe confirmar que sea el servicio a afectar.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tHABILITACION DEL SERVICIO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se documenta el procedimiento de Habilitación  y  marcación de la interface.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  \tCONFIRMACION  DE LA RECONEXION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se documenta la información de la interfaz y enrutamiento después de la suspensión.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Pruebas Operatividad)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  Observaciones.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'telefonia_IP_Centrex': /* CONFIGURACION IP CENTREX */
        return {
          'time': 1580504796180,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t* CONFIGURACION - IPCENREX *',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '@  RELACIÓN NUMERACIÓN Y EXTENSIONES',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'CIUDAD\tLDSET\tLINEA\tTIPO\tEXT\tTK',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '@  CONFIGURACIÓN CENTREX SOFTSW ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '@  ASOCIAR LINEAS AL CENTREX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHECK DE SALIDA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE CONFIGURA SERVICIO IPCENTREX CON LA SIGUIENTE RELACIÓN DE LÍNEAS',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'GRUPO: ###',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'CIUDAD\t\tNUMERO TELEFONICO DE CLARO\tEXTENSIÓN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Para Marcación diferente a las extensiones del IP-Centrex, el cliente debe anteponer el numero 9 a cualquier destino (9 + 748 0000 / 9 + 456 + 1 + 748 0000)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel tecnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'telefonia_IP_Centrex_NGN_SPG': /* CONFIGURACION IP CENTREX NGN-SPG */
        return {
          'time': 1580504915788,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t* CONFIGURACION - IPCENREX *',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '@  RELACIÓN NUMERACIÓN Y EXTENSIONES',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '// Documentar acorde a excel adjunto',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '@  CONFIGURACIÓN EXISTENTE SOFTSW / COMPTOR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '@  VERIFICACIÓN DISPONIBILIDAD RECURSOS SOBRE PLATAFORMASOFTSW',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  - GRUPO CENTREX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  - MARCACIONES PERMITIDAS',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  - SUSCRIPTORES / ABONADOS (SBRs)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  - SUSCRIPTORES / ABONADOS (DIDs)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '@  CONFIGURACIÓN DE RECURSOS SOFTSW / COMPTOR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  - GRUPO CENTREX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  - MARCACIONES PERMITIDAS',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  - SUSCRIPTORES / ABONADOS (SBRs)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  - SUSCRIPTORES / ABONADOS (DIDs)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tNO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tNO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tNO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tNO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tNO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHECK DE SALIDA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE CONFIGURA SERVICIO IPCENTREX CON LA SIGUIENTE RELACIÓN DE LÍNEAS',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'PLATAFORMA:\tNGN // SPG',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'TIPO:\t\tVSBR/MSBR/DID // IMUSER/E164DN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'GRUPO CENTREX:\t145',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'CIUDAD\t\tLINEA\tEXT',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '@  CONSIDERACIONES',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Para Marcación diferente a las extensiones del IP-Centrex, el cliente debe anteponer el numero 9 a cualquier destino (9 + 748 0000 / 9 + 456 + 1 + 748 0000)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel tecnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. VERIFICAR SALIDA Y ENTRADA DE LLAMADA HACIA LA PSTN (TELEFONÍA PÚBLICA) (GENERAR LLAMADAS CON EL NUMERO DE ESCAPE DEL CENTREX: 9)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. VERIFICAR SALIDA Y ENTRADA DE LLAMADA HACIA LAS EXTENSIONES CONFIGURADAS (CONFIRMAR QUE LAS LINEAS RELACIONADAS ESTEN OPERANDO)',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'novedad_Cambio_de_Acceso': /* CONFIGURACION CAMBIO DE ACCESO */
        return {
          'time': 1580505076942,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': 'CAMBIO DE ACCESO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tCONFIGURACIÓN PUERTO ACCESO (L2) (ACTUAL)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe dejar documentación total de la configuración del puerto de acceso. (Depende de cada plataforma).',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(En esta documentación se debe visualizar la marcación, las VLANs, Service-port o interfaces, el CAR)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN INTERFACE (L3)    (ACTUAL)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe documentar la información de la configuración de la interfaz creada en el PE y el enrutamiento según corresponda',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Ejemplo: Show runn interface Giga0/1/0.xxx, Show runn  vrf | inc ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN NUEVO ACCESO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe validar disponibilidad ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe dejar documentación total de la configuración del puerto de acceso. (depende de cada plataforma).',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(En esta documentación se debe visualizar la marcación, las VLANs, Service-port o interfaces, el CAR)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN INTERFACE (L3)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe documentar la información de la configuración de la interfaz creada en el PE y el enrutamiento según corresponda',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Ejemplo: Show runn interface Giga0/1/0.xxx, Show runn  vrf | inc ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACION ADICIONAL (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'En caso de que corresponda se debe documentar la información de la configuración realizada en interconexiones u otras redes.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tPENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se documenta información  de las configuraciones que quedaron pendientes o que se deben validar en la PEM.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tPDSR/PSR (ACTUALIZADA) ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe documentar la información de la PDSR, en donde debe estar actualizada la secuencia MPLS, red de acceso según configuración.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'En el caso de que no se pueda actualizar la PDSR por fallas se debe dejar documentado el correo de escalamientos.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tLIBERAR RECURSOS PEM',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe dejar documentación de la liberación de recursos a realizar tanto en el acceso como en el PE según corresponda esta (Esta información debe estar contenida en el script también), para esto se deben seguir los procedimientos establecidos para tal fin.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe documentar o diligenciar cada ítem según lo realizado en la configuración y adjuntar el script, show  run del CPE y minutograma.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel técnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe documentar la información a tener en cuenta o las pruebas a realizar en la PEM por el PIM y el IS',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Estos requerimientos técnicos se deben dejar en forma clara y resumida.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la información de acuerdo a lo solicitado en el check de configuración y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'novedad_Adicion_Telefonos_PBX_Administrada': /* ADICION TELEFONOS PBX ADMINISTRADA */
        return {
          'time': 1580505244137,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t*** ADICION TELEFONOS PBX ADMINISTRADA **',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tACTUALIZACION L2-L3',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tVALIDACION DISPONIBILIDAD DE IPS SEGMENTO PBX ADMINISTRADA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tCONFIGURACION EXTENSIONES',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©      CHEK LIST SALIDA\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\tNA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT Ejecutrado\t\tNA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\tNA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tNA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tNA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Plan de Trabajo\t\tNA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'PSR  actualizada segun  Ing detalle\t\tNA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'QoS PE\t\tNA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©Descripción de pruebas para entrega del servicio:\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. Verificacion de condiciones fisicas\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. Instalacion telefonos\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. Pruebas de PBX Administrada\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. Entrega de servicio y firma de actas',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'novedad_Adicion_o_Retiro_IP': /* ADICION-RETIRO IP */
        return {
          'time': 1580505348101,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\tNOVEDAD ADICIÓN/RETIRO IP',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'TIPO NOVEDAD:\tADICIÓN | RETIRO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  RED ASIGNADA:\t________/xx [CIDR]',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  VERIFICACIÓN DISPONIBILIDAD',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  CONFIGURACIÓN ACTUAL CAPA 3',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  CONFIGURACIÓN ACTUAL CPE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  PRUEBAS NAVEGACIÓN CONFIGURACIÓN ACTUAL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  MODIFICACIÓN ADICIÓN/RETIRO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  CONFIRMACIÓN ADICIÓN/RETIRO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  PRUEBAS NAVEGACIÓN DESPUES DE MODIFICACIÓN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  ACTUALIZACIÓN PDSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  OBSERVACIONES',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE REALIZA ADICION IP PÚBLICA SIN DESPLAZAMIENTO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE CONFIGURA SEGMENTO DE RED PUBLICA ____________',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE CREA RUTA EN EL PE Y SE AGREGA EN EL CPE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE VERIFICA CONECTIVIDAD SIN INCONVENIENTES',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RED ASIGNADA:\t\t________/xx [CIDR]',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'PUERTA DE ENLACE:\t______________',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'MÁSCARA:\t\t255.255.______',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'PRIMERA UTILIZABLE:\t______________',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'ÚLTIMA UTILIZABLE:\t______________',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'BROADCAST:\t\t______________',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'novedad_IP_Internet': /* NOVEDAD IP INTERNET */
        return {
          'time': 1580505510100,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\tDIRECCIONAMIENTO PUBLICO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RED:\t\t\t.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN INTERFACE (L3) - DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de configuración actual de subibnterface y enrutamiento de capa 3 - PE)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tVERIFICAR RESTRICCION CPE - DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de configuración actual de enrutamiento de CPE)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Consultar BGP y prefijos enseñados',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  DISPONIBILIDAD  IP ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  //Verificar disponibilidad de direccionameinto LAN público asignado',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Realizar el procedimiento contenido en el archivo socializado de disponibilidad de direccionamiento WAN/LAN (excel))',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  // Ejecutar (y reemplazar en la documentación) el siguiente procedimiento de verificación de enlistamiento de direccionamiento público',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '--------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'C&gt;nslookup x.x.x.x (ingresar IPs. a validar, se espera que el resultado sea estatic o dinamic)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Ingresar a: https://mxtoolbox.com/blacklists.aspx (Validar las IPs, Si aparece con X quiere decir que esta en lista negra)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Para mayor informacion ingresar al link: ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'https://embed.vidyard.com/share/GXWqFVMKnqj4PxC2yckste? ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '--------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN ACTUALIZADA EN CPE Y PE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de configuración modificada de subibnterface y enrutamiento de capa 3 - PE)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de configuración modificada de enrutamiento de CPE)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Consultar BGP y prefijos enseñados',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  VERIFICACION CONECTIVIDAD INTERNET',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de navegación con ip de origen el GW del nuevo segmento de direccionamiento público asignado)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba Ping &amp; Traza a Internet (Destino Nacional e Internacional)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'PING 190.XXX.XXX.XXX/XX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tPDSR (ACTUALIZADA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR/PSR actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCORREO DE NOTIFICACIÓN:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  OBSERVACIONES: ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE CONFIGURO DIRECCIONAMIENTO PUBLICO EN EL PUERTO XXX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE CONFIGURA IP PUBLICA XXX',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SE VERIFICA CONECTIVIDAD',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '---',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'comcel_Enlace_Directo': /* CONFIGURACION (ENLACE DIRECTO) v2.3 */
        return {
          'time': 1580505672695,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t*CONFIGURACION - ENLACE DIRECTO*',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Pruebas de configuración a nivel de L2 (se debe documentar el BW configurado)] ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar el TAB detalles en caso de ser GPON HUAWEI (PON o Ethernet)] ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En caso de ser GPON thales (PON o Ethernet) documentar el resultado del comando show vlan XXX] ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[En caso de ser GPON thales PON documentar el resultado del comando show onu run conf gpon-onu_1/X/X:X] ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SUB-INTERFACE/ENRUTAMIENTO PE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de configuración de subinterface en PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de configuración de enrutamiento en el PE]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si el enrutamiento es bgp en ASR, se debe documentar el route-policy y el prefix-set]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  NUMERACIÓN ASIGNADA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar numeración entregada en el check de configuración]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SIP',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de configuración de numeración en el SBC Y MSOFT BOGOTA Y MEDELLIN(documentar los resultados completos)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SS7',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t*MSOFT BOGOTA',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '\t*MSOFT MEDELLIN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION SBC ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de configuración de numeración en COMTOR de la móvil (documentar los resultados completos)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION ADICIONAL (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR/PSR completa y actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar plataforma intermedia a nivel de L2 Alcatel (BW, VLAN, Service ID)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar pendientes. ej. creación de túneles SDP.]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si se requiere módulo, documentar correo]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©      CHEK LIST TELEFONIA  -',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '"(NUEVO SERVICIO)   Check list Salida Telefonía (TK)"\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Equipo:\tRouter ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IOS del CPE:\tN/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Señalización:\tSIP centralizada (enlace directo)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Red loopback : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP loopback router: \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP loopback cliente (Aplica para SIP Centralizado) : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Red LAN (No aplica para SIP Centralizado): \tN/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP LAN  Router (No aplica para SIP Centralizado): \tN/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'IP LAN Planta (No aplica para SIP Centralizado): \tN/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'REGISTRO SBC (Aplica para SIP Centralizado): \tSBC ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'CANTIDAD CANALES:\tN/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'CANTIDAD DIDS:\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'BW por Línea:\tN/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'NÚMERO DE CABECERA:  ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'PERMISOS MARCACIÓN:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectación del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel técnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la información de acuerdo a lo solicitado en el check de configuración y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'comcel_3G': /* CONFIGURACION  COMCEL 2G-3G v2.2 */
        return {
          'time': 1580505841611,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t\tCONFIGURACION COMCEL',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PUERTO DE ACCESO ORIGEN (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Indicar puerto de acceso del origen]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PUERTO DE ACCESO DESTINO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Indicar puerto de acceso del destino]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO ORIGEN Y DESTINO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar configuracion en ALCATEL]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION ADICIONAL (QoS)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Pruebas de configuracion actual a nivel de L2 (se debe observar el BW configurado)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de los tuneles. ej. show service sdp 10044]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION INTERFACE EN VPRRN ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Prueba de las interfaces. ej. show router 300 interface "MEU6372"]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[garantizar la ruta O&amp;M para lo servicios 3G segun lo indicado en los parametros adjuntos. (tener en cuenta que si tiene las dos bandas 850 y 1900 se deben generar dos rutas).]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©   DIRECCIONAMIENTO/GESTION DEMARCADOR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Validacion de disponibilidad del direccionamiento WAN]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©   PRUEBAS A REALIZAR (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar pendientes. ej. creacion de tuneles SDP]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Si se requiere módulo, documentar correo]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©           PDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR/PSR completa y actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar plataforma intermedia a nivel de L2 Alcatel (ancho de banda, vlan, service id)]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel tecnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Nota: Se debe ingresar la informacion de acuerdo a lo solicitado en el check de configuracion y adjuntar soportes requeridos.',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'comcel_LTE': /* CONFIGURACION LTE */
        return {
          'time': 1580506067521,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\tCONFIGURACION COMCEL LTE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PUERTO DE ACCESO ORIGEN (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PUERTO DE ACCESO DESTINO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACIÓN PUERTO ACCESO ORIGEN Y DESTINO (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  CONFIGURACION ADICIONAL (QoS)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©   DIRECCIONAMIENTO/GESTION DEMARCADOR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©   DIRECCIONAMIENTO  LTE (Si APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se debe dejar información de  la configuración realizada en Alcatel, a continuación se deja ejemplo de la configuración a tener en cuenta  ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1.\tUtilizar el comando show router #vprn interface  "Nombre de la interface" para visualizar el direccionamiento de la interface del equipo de Alcatel, como se observa en el siguiente ejemplo:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:BAR-ALK-CN01# show router 1003400 interface "BAR.Villacountry-2 H2  eNB-CP"',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interface Table (Service: 1003400)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interface-Name                   Adm         Opr(v4/v6)  Mode    Port/SapId',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '   IP-Address                                                    PfxState',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-------------------------------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'BAR.Villacountry-2 H2  eNB-CP    Up          Down/--     VPRN    spoke-1401:31*',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '   10.227.25.150/29                                              n/a',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-------------------------------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interfaces : 1',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '* indicates that the corresponding row element may have been truncated.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:BAR-ALK-CN01# show router 1003400 interface "BAR.Villacountry-2 H2  eNB-UP',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interface Table (Service: 1003400)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interface-Name                   Adm         Opr(v4/v6)  Mode    Port/SapId',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '   IP-Address                                                    PfxState',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-------------------------------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'BAR.Villacountry-2 H2  eNB-UP    Up          Down/--     VPRN    spoke-1401:31*',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '   10.226.249.150/29                                             n/a',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-------------------------------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interfaces : 1',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '* indicates that the corresponding row element may have been truncated.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:BAR-ALK-CN01# ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:BAR-ALK-CN01# show router 1003600 interface "BAR.Villacountry-2 H2 eNB-O&amp;M"',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interface Table (Service: 1003600)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interface-Name                   Adm         Opr(v4/v6)  Mode    Port/SapId',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '   IP-Address                                                    PfxState',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-------------------------------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'BAR.Villacountry-2 H2 eNB-O&amp;M    Up          Down/--     VPRN    spoke-1401:31*',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '   10.226.185.150/29                                             n/a',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-------------------------------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interfaces : 1',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '* indicates that the corresponding row element may have been truncated.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:BAR-ALK-CN01# ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2.Cofirmar la configuracion correcta en el nodo de acceso del protocolo OSPF con el siguiente comando:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '***show router ospf interface | match Nombre de la interface',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Ejemplo:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:AAG-BAR.WTC-C1# show router ospf interface | match "BAR.Villacountry',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'BAR.Villacountry-2 H* 0.0.0.53        0.0.0.0         0.0.0.0         Up   Down',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:AAG-BAR.WTC-C1# ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3.Confirmar la configuracion del direccionamiento ip en el sincronismo con el siguiente comando:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '******show router interface  "Nombre de la interface"',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Ejemplo',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:AAG-BAR.WTC-C1# show router interface "BAR.Villacountry-2 H2  eNB-SYNC"',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interface Table (Router: Base)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interface-Name                   Adm         Opr(v4/v6)  Mode    Port/SapId',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '   IP-Address                                                    PfxState',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-------------------------------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'BAR.Villacountry-2 H2  eNB-SYNC  Up          Down/--     IES     4/1/6:3500',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '   10.226.199.5/30                                               n/a',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '-------------------------------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Interfaces : 1',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:AAG-BAR.WTC-C1# ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©   PRUEBAS A REALIZAR (SI APLICA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©           PDSR/PSR',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'ORIGEN',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© CHEK LIST CONFIGURACION\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Adjuntar en OTP soportes requeridos para entrega:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Minutograma ejecutado\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'SCRIPT ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'RFC  ejecutado\t\t\t\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Pruebas de /los servicio(s) actual(es)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Configuración actual del  CPE (sh run)\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Hay afectacion del servicio\t\t\t\tSI/NO\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se configura por  activador (HPSA)              SI/  NO/  NO APLICA (JUSTIFICACION)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': ' " EN  CASO DE  ERROR  DE  HPSA DOCUMENTAR  EL  CORREO"\t\t\t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© Descripción de requerimientos para entrega del servicio (A nivel tecnico):',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '----------------------------------------------------------',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '1. ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '2. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '3. : \t',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '4. : ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '#. : ',
              },
            },
          ],
          'version': '2.16.1',
        };
      case 'Cambio_de_Velocidad_BW_PC': /* NOVEDAD CAMBIO BW - PC */
        return {
          'time': 1580506300860,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\tMODIFICACION ANCHO DE BANDA ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '© \tNuevo Ancho de banda:\t.',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Registro de capacidad en documentación de tab de detalles)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN PUERTO ACCESO(L2) - DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de capacidad de capa 2 - SW acceso)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN INTERFACE (L3) - DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de capacidad de capa 3 - PE)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tVERIFICAR RESTRICCION CPE - DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de capacidad del CPE)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  Consultar Policy-maps | rate-limit ajustados en interfaces (WAN-LAN) del CPE',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tACTUALIZACIÓN PUERTO ACCESO (L2-L3) - DESTINO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de modificación de capacidad de capa 2-3 - SW acceso y PE)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  // En caso de que se requiera cambio de plataforma, documentar disponibilidad y reserva de los nuevos recursos',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '=======================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '==    Extremo Origen | PuntoCentral  ==',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '=======================================',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN PUERTO ACCESO(L2) - PUNTO ORIGEN (PC)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'N/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCONFIGURACIÓN INTERFACE (L3) - PUNTO ORIGEN (PC)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'N/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tVERIFICAR RESTRICCION CPE - PUNTO ORIGEN (PC)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'N/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tACTUALIZACION BW (L2-L3 / CPE) - PUNTO ORIGEN (PC)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '(Evidencia de modificación de capacidad de capa 2-3 - SW acceso y PE)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '  // En caso de que se requiera cambio de plataforma, documentar disponibilidad y reserva de los nuevos recursos',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'N/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tPENDIENTES DE LA CONFIGURACION',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'N/A',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tPDSR/PSR (ACTUALIZADA)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '[Documentar PDSR/PSR actualizada]', /*seIncluye  */
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tOBSERVACIONES:',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Requiere Cambio de CPE:\t\tSI/NO',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'Se adjunta grafico de saturación (remota)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©\tCORREO DE NOTIFICACIÓN:',
              },
            },
          ],
          'version': '2.16.1',
        };
      default:
        return {
          'time': 1586280899747,
          'blocks': [
            {
              'type': 'paragraph',
              'data': {
                'text': '\t\t\t\tPL ETHERNET',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '©  PUERTO DE ACCESO ORIGEN (L2)',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': 'A:AAG-CHICONORTE-C1# show port 3/1/11 description ',
              },
            },
            {
              'type': 'paragraph',
              'data': {
                'text': '===============================================================================',
              },
            },
          ],
          'version': '2.16.1',
        };
    }

  };

  componentDidMount() {
    // this.editorInstance; // access editor-js
  }
  ;
  render() {
    return (
      <>
        {/* eslint-disable-next-line react/destructuring-assignment */}
        {this.props.modalOpen ?
          // eslint-disable-next-line react/destructuring-assignment
          (this.props.CkList ?
            (
              <EditorJs
                tools={EDITOR_JS_TOOLS}
                onChange={() => { /* console.log('Something is changing!!'); */ this.handleSave(); }}
                // eslint-disable-next-line no-return-assign
                instanceRef={(instance) => this.editorInstance = instance}
                onData={(data) => console.log(data)}
                // eslint-disable-next-line react/destructuring-assignment
                data={this.props.Ckformat ? (this.props.Ckformat.format ? (this.props.Ckformat.format) : (this.props.CkList ? this.activeEditor(this.props.modalOpen && this.props.CkList.tipoServicio) : {})) : (this.props.CkList ? this.activeEditor(this.props.modalOpen && this.props.CkList.tipoServicio) : {})}
              />
            ) : <h1>Cargando.....</h1>) : this.save()}
      </>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    CkList: state.checklistState.CkList,
    Ckformat: state.checklistState.Ckformat,
    modalOpen: state.checklistState.modalOpen,
  };
};

const mapDispatchToProps = {
  saveCkList,
  saveCkFormat,
};

export default connect(mapStateToProps, mapDispatchToProps)(TextEditor);

